--
-- 
--

-- pour permettre l'affichage immediate
io.stdout:setvbuf("no")

a,b,c=...

print("MQTT",a,b,c)

local mqtt=require("mqtt")
--local ioloop=require("ioloop")


local client = mqtt.client{
	uri = "broker.hivemq.com",
	username = "love2d",
	clean = true,
}
print("created MQTT client", client)

client:on {
	connect = function(connack)
		if connack.rc ~= 0 then
			print("connection to broker failed:", connack:reason_string(), connack)
			return
		end
		print("connected:", connack) -- successful connection

		-- subscribe to test topic and publish message after it
		client:subscribe{ topic="love2d/#", qos=1, callback=function(suback)
        print("subscribed:", suback)
      end
    }
	end,

	message = function(msg)
		assert(client:acknowledge(msg))
		print("received:", msg)
    if channel then channel:push(msg.topic.." : "..msg.payload) end
	end,

	error = function(err)
		print("MQTT client error:", err)
	end,
}


channel=love.thread.getChannel("msg")
channel_pub=love.thread.getChannel("pub")

function pub()
		if channel_pub then
			local t=channel_pub:pop()
			if t then
				print("topic "..t.topic.." payload "..t.payload)
				client:publish(t)
			end
		end
end

mqtt.run_ioloop(client,pub)





