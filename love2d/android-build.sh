#!/bin/bash

cd asteroid
zip -9 -r ../asteroid.love .
cd ..


mkdir -p android
cd android

if [ ! -f love-11.3-android-embed.apk ]; then
	wget https://github.com/love2d/love/releases/download/11.3/love-11.3-android-embed.apk
fi
if [ ! -f uber-apk-signer-1.1.0.jar ]; then
	wget https://github.com/patrickfav/uber-apk-signer/releases/download/v1.1.0/uber-apk-signer-1.1.0.jar
fi
if [ ! -d love_decoded ]; then
	apktool d -s -o love_decoded love-11.3-android-embed.apk
fi


mkdir -p love_decoded/assets
mv ../asteroid.love love_decoded/assets/game.love


export GamePackageName="org.seboid.asteroid"
export GameVersionCode="10"
export GameVersionSemantic="0.1"
export GameName="Asteroid"
envsubst <../AndroidManifest.xml-template >love_decoded/AndroidManifest.xml


apktool b -o asteroid.apk love_decoded
java -jar uber-apk-signer-1.1.0.jar -a asteroid.apk
#install dans le telephone
adb install asteroid-aligned-debugSigned.apk
cd ..
# start!
adb shell am start -n org.seboid.asteroid/org.love2d.android.GameActivity



