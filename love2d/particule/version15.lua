

--
-- Closure!!!!!
--
-- suite de la version5.
--
-- On ajoute une duree de vie, avec un fade a la fin
--

function particule()
  local self={}
  -- attributes prives (cachés à tous)
  local x=love.math.random(0,w)
  local y=love.math.random(0,h)
  local vx=love.math.random(-30,30)
  local vy=love.math.random(-30,30)
  local red=love.math.random(0,1)
  local green=love.math.random(0,1)
  local blue=love.math.random(0,1)
  local vie=love.math.random(3,10) -- ce qu'il reste de vie, entre 3 et 10 seconde
  -- attribut public, doit etre placé dans la table (ici, blub)
  self.blub=1234
  -- fonctions dans la table: draw et update.
  -- ils utilisent les attributs (variables locales)
  self.enVie=function() return vie>=0 end
  self.draw=function ()
    -- si on est mort, on ne desine rien
    if vie<=0 then return end
    -- dans la derniere seconde de vie, on fait un fadeout
    local fade=math.min(vie,1)
    love.graphics.setColor(red*fade,green*fade,blue*fade)
    love.graphics.points({x,y})
  end
  self.update=function (dt)
    vie=vie-dt -- le temps passe
    x=x+vx*dt
    y=y+vy*dt
    if (x>w and vx>0) or (x<0 and vx<0) then vx=-vx end
    if (y>h and vy>0) or (y<0 and vy<0) then vx=-vy end
  end
  return self
end


p={}

function love.load()
  love.window.setMode(640,480)
  w,h=love.graphics.getDimensions()
  
  for i=1,200 do
    p[i]=particule()
  end
end

function love.draw()
  love.graphics.setPointSize(5)
  for i=1,#p do
    p[i].draw()
  end
end

function love.update(dt)
    for i=1,#p do
      p[i].update(dt)
    end
    -- que faire avec les particules mortes?
    -- si on veut les enlever du tableau, il faut toujours enlever le dernier element du tableau.
    -- donc on va echanger une particule morte avec la derniere particule, puis eliminer le bout du tableau
    -- on ne peut pas utiliser un for ici parce que la taille du tableau va changer pendant la boucle...
    i=1
    while i<=#p do
      if not p[i].enVie() then
        p[i]=p[#p]
        p[#p]=nil -- elimine!
        print("particule "..i.." eliminee, il reste "..#p.." particules")
      end
      i=i+1
    end
    -- et si on veut qu'une particule apparaisse si on a moins de 200 particules?
    if #p<200 then
      if love.math.random(0,1)<dt then
        print("nouvelle particule!!!")
        --table.insert(p,particule())
        p[#p+1]=particule()
      end
    end
end

