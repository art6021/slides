--
-- Mettre en premier, avec une seule particule... avant de faire des tableaux de particules
--
-- info -> self dans une version precedente...
--

-- cohesion + , couplage -

--
-- Les fonctions liées aux particules devraient 6etre séparées
--
-- on va donc créer particuleDraw et particuleUpdate
--

----- la particule -----

function particule()
  local self={}
  self.x=love.math.random(0,w)
  self.y=love.math.random(0,h)
  self.vx=love.math.random(-30,30)
  self.vy=love.math.random(-30,30)
  self.red=love.math.random(0,1)
  self.green=love.math.random(0,1)
  self.blue=love.math.random(0,1)
  return self
end

function particuleDraw(self)
  love.graphics.setColor(self.red,self.green,self.blue)
  love.graphics.points({self.x,self.y})
end

function particuleUpdate(self,dt)
  self.x=self.x+self.vx*dt
  self.y=self.y+self.vy*dt
  
  if (self.x>w and self.vx>0) or (self.x<0 and self.vx<0) then self.vx=-self.vx end
  if (self.y>h and self.vy>0) or (self.y<0 and self.vy<0) then self.vx=-self.vy end
end

--------- programme principal --------

-- tableau de particules
p={}

function love.load()
  love.window.setMode(640,480)
  w,h=love.graphics.getDimensions()
  
  for i=1,200 do
    p[i]=particule()
  end
end

function love.draw()
  love.graphics.setPointSize(5)
  for i=1,#p do
    particuleDraw(p[i])
  end
end

function love.update(dt)
    for i=1,#p do
      particuleUpdate(p[i],dt)
    end
end

