
-- pour permettre l'affichage immediate
io.stdout:setvbuf("no")


-- version 1
-- tableaux séparés pour les x,y,vx,vy,red,green,blue
--require("version1")

-- version 2
-- objets composite simple pour les propriétés
--require("version2")

-- version 3
-- objets avec fonctions comme proprietes
--require("version3")

-- version 4
-- un attribut peut etre une fonction!
--require("version4")

-- version 5
-- meme version avec les closure
-- require("version5")

-- version 6
-- meme version avec les metatable
--require("version6")

-- version 15
-- comme v5 (closure), avec duree de vie
-- particule disparaissent, et naissent
--require("version15")

-- version 16
-- comme v15, une particule peut se diviser
require("version16")

-- version "nuage"

