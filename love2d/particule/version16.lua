

--
-- Closure!!!!!
--
-- suite de la version5.
--
-- On ajoute une duree de vie, avec un fade a la fin
--

-- depart peut etre une dictionnaire avec x,y,vx,vy,red,green,blue pour definir des valeurs de depart

function particule(depart)
  local self={}
  -- attributes prives (cachés à tous)
  local x=(depart and depart.x) or love.math.random(0,w)
  local y=(depart and depart.y) or math.random(0,h)
  local vx=love.math.random(-30,30)
  local vy=love.math.random(-30,30)
  local red=(depart and depart.red) or math.random()
  local green=(depart and depart.green) or love.math.random()
  local blue=(depart and depart.blue) or love.math.random()
  local vie=love.math.random()*7+3 -- ce qu'il reste de vie, entre 3 et 10 seconde
  -- attribut public, doit etre placé dans la table (ici, blub)
  self.blub=1234
  -- fonctions dans la table: draw et update.
  -- ils utilisent les attributs (variables locales)
  self.enVie=function() return vie>=0 end
  self.draw=function ()
    -- si on est mort, on ne desine rien
    if vie<=0 then return end
    -- dans la derniere seconde de vie, on fait un fadeout
    local fade=math.min(vie,1)
    love.graphics.setColor(red*fade,green*fade,blue*fade)
    love.graphics.points({x,y})
  end
  self.update=function (dt)
    vie=vie-dt -- le temps passe
    x=x+vx*dt
    y=y+vy*dt
    if (x>w and vx>0) or (x<0 and vx<0) then vx=-vx end
    if (y>h and vy>0) or (y<0 and vy<0) then vx=-vy end
  end
  -- retourne un dictionnaire avec de l'info sur la particule
  self.getInfo=function() return {x=x,y=y,red=red,green=green,blue=blue} end
  return self
end


p={}

function love.load()
  love.window.setMode(640,480)
  w,h=love.graphics.getDimensions()
  
  for i=1,200 do
    p[i]=particule()
  end
end

function love.draw()
  love.graphics.setPointSize(5)
  for i=1,#p do
    p[i].draw()
  end
end

function love.update(dt)
    for i=1,#p do
      p[i].update(dt)
    end
    -- que faire avec les particules mortes?
    -- si on veut les enlever du tableau, il faut toujours enlever le dernier element du tableau.
    -- donc on va echanger une particule morte avec la derniere particule, puis eliminer le bout du tableau
    -- on ne peut pas utiliser un for ici parce que la taille du tableau va changer pendant la boucle...
    i=1
    while i<=#p do
      if not p[i].enVie() then
        p[i]=p[#p]
        p[#p]=nil -- elimine!
        print("particule "..i.." eliminee, il reste "..#p.." particules")
      end
      i=i+1
    end
    -- si on veut qu'une particule se divise en deux nouvelles particules de la meme couleur, on fait quoi?
    if #p<199 then
      if love.math.random(0,1)<dt then
        local i=love.math.random(1,#p)
        if p[i].enVie() then
          print("nouvelle division!!!")
          p[#p+1]=particule(p[i].getInfo())
          p[#p+1]=particule(p[i].getInfo())
        end
      end
    end
end

