

--
-- metatables!
--
-- avec les metatables, on peut partager des fonctions et attributs communs.
-- Une particule ne va donc pas définir ces fonctions/attributs, mais
-- simplement indiquer (avec setmetatable) qu'il faut aller voir dans une autre table (ici Particule)
-- si une fonction ou attribut n'est pas dans la table de la particule (qui ne contient que x,y,..., les attributs spécifiques à ne particule en particulier
--
-- La metatable contient une information qui indique qu'il faut aller voir la table Particule si on ne trouve pas
-- dans le dictionnaire auquel la metatable est attachée.
--

Particule={}
Particule.__index=Particule


function Particule.new()
  local self={}
  setmetatable(self,Particule)

  self.x=love.math.random(0,w)
  self.y=love.math.random(0,h)
  self.vx=love.math.random(-30,30)
  self.vy=love.math.random(-30,30)
  self.red=love.math.random(0,1)
  self.green=love.math.random(0,1)
  self.blue=love.math.random(0,1)

  return self
end

function Particule:draw()
    love.graphics.setColor(self.red,self.green,self.blue)
    love.graphics.points({self.x,self.y})
  end
  
function Particule:update(dt)
    self.x=self.x+self.vx*dt
    self.y=self.y+self.vy*dt
    if (self.x>w and self.vx>0) or (self.x<0 and self.vx<0) then self.vx=-self.vx end
    if (self.y>h and self.vy>0) or (self.y<0 and self.vy<0) then self.vx=-self.vy end
end



p={}

function love.load()
  love.window.setMode(640,480)
  w,h=love.graphics.getDimensions()
  
  for i=1,200 do
    p[i]=Particule.new()
  end
end

function love.draw()
  love.graphics.setPointSize(5)
  for i=1,#p do
    p[i]:draw()
  end
  print(p[1].x,p[1].y)
end

function love.update(dt)
    for i=1,#p do
      p[i]:update(dt)
    end
end

