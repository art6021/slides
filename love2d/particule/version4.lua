

--
-- Les fonctions liées aux particules sont aussi des attributs...
-- on peut les mettre avec les autres attributs (x,y,...)
--
-- Dans le dictionnaire d'une particule, on a les données,
-- et ce qu'on peut faire avec les données (les fonctions)
--


--
-- dans un tableau, on place les proprietes,
-- mais on peut aussi placer des fonctions
--

function particule()
  local self={}
  -- les attributs "données"
  self.x=love.math.random(0,w)
  self.y=love.math.random(0,h)
  self.vx=love.math.random(-30,30)
  self.vy=love.math.random(-30,30)
  self.red=love.math.random(0,1)
  self.green=love.math.random(0,1)
  self.blue=love.math.random(0,1)
  -- les attributs "fonctions" qui utilisent les données
  self.draw=function (self)
    love.graphics.setColor(self.red,self.green,self.blue)
    love.graphics.points({self.x,self.y})
  end
  self.update=function (self,dt)
    self.x=self.x+self.vx*dt
    self.y=self.y+self.vy*dt
    if (self.x>w and self.vx>0) or (self.x<0 and self.vx<0) then self.vx=-self.vx end
    if (self.y>h and self.vy>0) or (self.y<0 and self.vy<0) then self.vx=-self.vy end
  end
  return self
end



p={}

function love.load()
  love.window.setMode(640,480)
  w,h=love.graphics.getDimensions()
  
  for i=1,200 do
    p[i]=particule()
  end
end

function love.draw()
  love.graphics.setPointSize(5)
  for i=1,#p do
    --p[i].draw(p[i])
    p[i]:draw()
  end
end

function love.update(dt)
    for i=1,#p do
      --p[i].update(p[i],dt)
      p[i]:update(dt)
    end
end

