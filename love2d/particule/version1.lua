
--
-- La base
--
-- chaque attribut (x,y,...) est dans un tableau séparé.
--
-- peu de cohésion (les attributs sont séparés)
-- 


x={}
y={}
vx={}
vy={}
red={}
green={}
blue={}

function love.load()
  love.window.setMode(640,480)
  w,h=love.graphics.getDimensions()
  
  for i=1,200 do
    x[i]=love.math.random(0,w)
    y[i]=love.math.random(0,h)
    vx[i]=love.math.random(-30,30)
    vy[i]=love.math.random(-30,30)
    red[i]=love.math.random(0,1)
    green[i]=love.math.random(0,1)
    blue[i]=love.math.random(0,1)
  end
end

function love.draw()
  love.graphics.setPointSize(5)
  for i=1,#x do
    love.graphics.setColor(red[i],green[i],blue[i])
    love.graphics.points({x[i],y[i]})
  end
end

function love.update(dt)
    for i=1,#x do
      x[i]=x[i]+vx[i]*dt
      y[i]=y[i]+vy[i]*dt
    end
    
    for i=1,#x do
      if (x[i]>w and vx[i]>0) or (x[i]<0 and vx[i]<0) then vx[i]=-vx[i] end
      if (y[i]>h and vy[i]>0) or (y[i]<0 and vy[i]<0) then vy[i]=-vy[i] end
    end
end

