

--
-- Closure!!!!!
--
-- Ici on va faire un saut dans vide....
--
-- Normalement, une variable locale disparait à la fin d'une fonction.
--
-- Mais si une fonction utilise une variable locale d'une fonction parente, alors cette
-- variable reste "en vie" tant que la fonction existe.
--
-- Ici, dans particule(), on va créer des variables locales pour les attributs (x,y,vx,..)
-- et on va définir deux fonctions (draw et update) qui utilisent ces variables.
-- quand on va retourner la table qui contient les deux fonctions, les attributs (variables locales) vont
-- rester "en vie" sans faire partie de la table.
-- Ils sont "cachés" pour tout le monde, sauf pour les fonctions qui les utilisent, draw et update
--

function particule()
  local self={}
  -- attributes prives (cachés à tous)
  local x=love.math.random(0,w)
  local y=love.math.random(0,h)
  local vx=love.math.random(-30,30)
  local vy=love.math.random(-30,30)
  local red=love.math.random(0,1)
  local green=love.math.random(0,1)
  local blue=love.math.random(0,1)
  -- attribut public, doit etre placé dans la table (ici, blub)
  self.blub=1234
  -- fonctions dans la table: draw et update.
  -- ils utilisent les attributs (variables locales)
  self.draw=function ()
    love.graphics.setColor(red,green,blue)
    love.graphics.points({x,y})
  end
  self.update=function (dt)
    x=x+vx*dt
    y=y+vy*dt
    if (x>w and vx>0) or (x<0 and vx<0) then vx=-vx end
    if (y>h and vy>0) or (y<0 and vy<0) then vx=-vy end
  end
  return self
end


p={}

function love.load()
  love.window.setMode(640,480)
  w,h=love.graphics.getDimensions()
  
  for i=1,200 do
    p[i]=particule()
  end
end

function love.draw()
  love.graphics.setPointSize(5)
  for i=1,#p do
    p[i].draw()
  end
end

function love.update(dt)
    for i=1,#p do
      p[i].update(dt)
    end
end

