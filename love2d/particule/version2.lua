--
-- dictionnaires
--
-- On place tous les attributs (x,y,...) ensemble dans un dictionnaire
--
-- meilleure cohésion!
--


function particule()
  local info={}
  --intro["x"]=...
  info.x=love.math.random(0,w)
  info.y=love.math.random(0,h)
  info.vx=love.math.random(-30,30)
  info.vy=love.math.random(-30,30)
  info.red=love.math.random(0,1)
  info.green=love.math.random(0,1)
  info.blue=love.math.random(0,1)
  return info
end

-- un seul tableau contient les particules
-- plus facile a gérer!
p={}

function love.load()
  love.window.setMode(640,480)
  w,h=love.graphics.getDimensions()
  
  for i=1,200 do
    p[i]=particule()
  end
end

function love.draw()
  love.graphics.setPointSize(5)
  for i=1,#p do
    love.graphics.setColor(p[i].red,p[i].green,p[i].blue)
    love.graphics.points({p[i].x,p[i].y})
  end
end

function love.update(dt)
    for i=1,#p do
      p[i].x=p[i].x+p[i].vx*dt
      p[i].y=p[i].y+p[i].vy*dt
    end
    
    for i=1,#p do
      if (p[i].x>w and p[i].vx>0) or (p[i].x<0 and p[i].vx<0) then p[i].vx=-p[i].vx end
      if (p[i].y>h and p[i].vy>0) or (p[i].y<0 and p[i].vy<0) then p[i].vx=-p[i].vy end
    end
end

