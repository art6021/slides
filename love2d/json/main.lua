
local socket = require "socket.http"
local ltn12 = require "ltn12"
local io = require "io"
local json = require "json"

url="https://api.openweathermap.org/data/2.5/weather?q=Montreal,ca&units=metric&lang=fr&appid=61f3479bb5de294b45e5283b498cfce2"

--url="https://api.openweathermap.org/data/2.5/onecall?lat=45.51&lon=-73.59&units=metric&lang=fr&appid=61f3479bb5de294b45e5283b498cfce2"

t={}
client,r,c,h = socket.request{url = url, sink = ltn12.sink.table(t)}
print("r",r,#t)
data=table.concat(t)
print(data)
j=json.decode(data)

function imprime(j,indent)
		for k,v in pairs(j) do
				for i=1,(indent or 0) do io.write(" ") end
				print(k,"=",v)
				if type(v)=="table" then imprime(v,(indent or 0)+4) end
		end
end

imprime(j,8)


