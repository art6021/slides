--
-- damier 8x8
--

io.stdout:setvbuf("no")

function love.load()
  love.window.setMode(400,400)
  love.graphics.captureScreenshot("escalier3.png")
end

--[[
function love.draw()
  for i=0,400,100 do
    for j=50,400,100 do
      --love.graphics.rectangle("fill",i,j,50,50)
    end
  end
  for i=50,400,100 do
    for j=0,400,100 do
      love.graphics.rectangle("fill",i,j,50,50)
    end
  end
end
]]

--[[
function love.draw()
  local t=400/8
  love.graphics.setColor(1,1,1)
  for j=0,7 do
    for i=0,7 do
      if (i+j)%2 == 0 then
        love.graphics.rectangle("fill",i*t,j*t,t,t)
      end
    end
  end
end
]]

--[[
function love.draw()
    local t=400/8
    for i=0,400,2*t do
      for j=0,400,2*t do
        love.graphics.origin()
        love.graphics.translate(i,j)
        love.graphics.rectangle("fill",0,0,t,t)
        love.graphics.rectangle("fill",t,t,t,t)
      end
    end
end
--]]

--[[
function love.draw()
  local t=400/8
  for i=0,400,t do
    love.graphics.rectangle("fill",i,i,t,t)
  end
end
]]

function love.draw()
  local t=400/8
  for i=0,400,t do
    love.graphics.setColor(1,1,1)
    love.graphics.rectangle("fill",i,i,t,t)
    love.graphics.setColor(1,0,0)
    for j=0,i-t,t do
      love.graphics.circle("fill",i+t/2,j+t/2,t/2)
    end    
  end
end

