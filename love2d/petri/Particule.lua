--
-- particule de bouffe ou de poison ou autre choses...
--
require("utile")

local stylesInfo={
  bouffe={couleur={0,1,0}},
  poison={couleur={1,0,0}}
  }
local styles={}
for k,v in pairs(stylesInfo) do table.insert(styles,k) end
      
      
      
      
function Particule(info)
  local self={}
  local t=(info and info.t) or aleatoire(4,8)
  local x,y
  repeat
    x=(info and info.x) or aleatoire(-1000,1000)
    y=(info and info.y) or aleatoire(-1000,1000)
  until norme(x,y)+t<999
  local nom=(info and info.nom) or styles[love.math.random(1,#styles)]
  local age=aleatoire(0,1)
  local vx = (info and info.vx) or aleatoire(-30,30)
  local vy = (info and info.vy) or aleatoire(-30,30)
  local tmax= (info and info.tmax) or t  -- taille max

  function self.draw()
    a=cycle(age,0.8,1)
    local info=stylesInfo[nom]
    local c=info.couleur
    love.graphics.setColor(c[1],c[2],c[3],a)
    love.graphics.circle("fill",x,y,t)
  end
  
  function self.pousse(px,py,force)
    local dx,dy=px-x,py-y
    local n=norme(dx,dy)
    if n==0 then return end
    vx,vy = vx+dx/n*force,vy+dy/n*force
  end
  
  function self.update(dt)
    age=age+dt
    x=x+vx*dt
    y=y+vy*dt
    
    -- grossissement
    t=t*0.99+0.01*tmax
    
    
    -- rebond sur le petri
    local L=norme(x,y)
    local dx,dy = x/L,y/L
    local sens=vx*dx+vy*dy  -- + = changement de signe
    if L>=1000-t and sens>0 then
      -- portion de vx,vy PAS projetee sur dx,dy
      local sens2=vx*dy-vy*dx
      -- normalement la vitesse est : (dx,dy)*sens + (dy,-dx)*sens2
      -- rebond  -(dx,dy)*sens + (dy,-dx)*sens2
      vx,vy = -dx*sens+dy*sens2 , -dy*sens-dx*sens2
    end

  end


  function self.distance(mx,my)
    local dx,dy = mx-x,my-y
    return math.sqrt(dx*dx+dy*dy)
  end
  
  -- la particule (x,y,t) intersecte le cercle (mx,my,mt)?
  -- on dit que ca intersecte quand un des centre est dans l'autre particule
  function self.intersecte(mx,my,mt)
    local dx,dy = mx-x,my-y
    local d=math.sqrt(dx*dx+dy*dy)
    return d<t or d<mt
  end
  
  
  function self.getNom() return nom end
  function self.getTaille() return t end
  function self.getXYT() return x,y,t end

  return self
end



