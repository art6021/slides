--
-- fond
--
require("utile")


function Fond(w,h)
  local self={}
  local P={}
  for i=1,400 do
    P[i]={x=aleatoire(-1000,1000),y=aleatoire(-1000,1000),
      r=aleatoire(0.1,3),
      couleur={HSL(0.6,aleatoire(0.5,1),aleatoire(0.25,1))}}
  end
  
  
  function self.draw()
    for i=1,#P do
      love.graphics.setColor(P[i].couleur)
      love.graphics.circle("fill",P[i].x,P[i].y,P[i].r)
    end
  end
  
  function self.update()
  end  
  
  return self
end




