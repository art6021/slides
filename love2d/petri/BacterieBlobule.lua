--
-- bacterie
--
require("utile")
require("Animation")

function BacterieBlobule(info)
  local self={}
  local x=(info and info.x) or 0
  local y=(info and info.y) or 0
  local t=(info and info.t) or 50 -- taille
  local c={couleurRandom()}
  local age=0
  local ouvre=nil -- animation pour "ouvrir"
  local ferme=nil -- animation pour "fermer" quand on pousse
  local shake=nil -- juste une duree de shake
  local angle=0 -- angle pour l'animation, 90 a 160 typique
  local vx,vy = 0,0

  function self.draw()
    love.graphics.push()
    love.graphics.translate(x,y)
    love.graphics.rotate(age/30)
    
    -- rond de base
    local a=cycle(age,0.4,0.8)
    love.graphics.setColor(c[1],c[2],c[3],a)
    love.graphics.circle("fill",0,0,t)
    
    -- ici on fait ce qu on veut....
    --
    -- angle 90 : ouvert,   160: ferme
    -- si move<0 on veut atteindre 90 a move=0
    local tr=love.math.newTransform()
    if ouvre then
      angle=ouvre.val
      if ouvre.fini() then ouvre=nil end
    elseif ferme then
      angle=ferme.val
      if ferme.fini() then ferme=nil end -- jamais mais bon...
    end
    if shake then
      local dx,dy=aleatoire(-4,4),aleatoire(-4,4)
      if norme(dx,dy)<20 then
        love.graphics.translate(dx,dy)
      end
    end
    
    tr:rotate(angle*math.pi/180)
    tr:scale(1.5)
    love.graphics.setColor(1,1,1)
    love.graphics.setLineWidth(1)
    local x1,y1=t,0
    local x2,y2=tr:transformPoint(x1,y1)
    --love.graphics.rotate(age/30)
    for i=0,349,15 do
      love.graphics.rotate(15*math.pi/180)
      love.graphics.line(x1,y1,x2,y2)
    end    
    love.graphics.pop()
  end
  
  -- donne un peu d'info sur la bacterie...
  function self.infoTexte()
          return "Sebastianium Lua",
            string.format("Taille %d",math.floor(t)),
            string.format("Vitesse (%6.1f,%6.1f)",vx,vy),
            string.format("Position (%6.1f,%6.1f)",x,y)
  end
  
  
  function self.contient(px,py)
    return norme(x-px,y-py)<t
  end
  
  
  -- averti la bacterie qu'elle va faire un move m dans un certain temps (delai)
  function self.prepare(delais,nom,px,py,force)
    if nom=="pousse" then
      ouvre=Animation(angle,90,delais)
      ferme=nil
    else
      shake=delais
    end
    print("prepare move "..nom.." dans "..delais)
  end
  
  
  function self.pousse(px,py,force)
    print("bacterie pousse!")
    local dx,dy=px-x,py-y
    local n=norme(dx,dy)
    if n==0 then return end
    vx,vy = vx+dx/n*force,vy+dy/n*force
    ouvre=nil
    ferme=Animation(angle,170,nil,0.9) -- pas de temps de fin, exponentiel
  end
  
  function self.update(dt)
    age=age+dt
    x=x+vx*dt
    y=y+vy*dt
    if norme(vx,vy)>10 then
      vx=vx*0.95
      vy=vy*0.95
    end
    
    -- animation 
    if ouvre then ouvre.update(dt) end
    if ferme then ferme.update(dt) end
    
    -- bord du petri
    local L=norme(x,y)
    if L>1000-t then
      vx,vy=0,0
      x,y = x/L*(1000-t),y/L*(1000-t)
    end
    
    if shake then
      shake=shake-dt
      if shake<0 then shake=nil end
    end
    
  end
  
  -- verifie qu'on est pas en collision avec le cercle (centre xy, rayon t)
  -- ajuste seulement la vitesse.
  -- collision non elastique, avec leger rebond pour empecher la superposition
  function self.obstacle(ox,oy,ot)
    local dx,dy=ox-x,oy-y
    local d=norme(dx,dy)
    if d>t+ot then return end
    -- collision! projete vx,vy sur dy,dx pour la composante perpendiculaire
    dx,dy=dx/d,dy/d
    -- d contient le rebond... juste pour eliminer la superposition
    d=math.min(0,d-t-ot)
    local v=vx*dy-vy*dx
    vx,vy=dx*d+dy*v,dy*d-dx*v
  end
  
  
  function self.grossi(delta)
    t=math.max(5,t+delta)
  end
  
  --
  -- on nous donne un portrait du monde...
  -- on doit donne un "move"
  --
  function self.conscience(carte)
    if t>100 then
      return {move="poison",x=x+aleatoire(-1000,1000),y=y+aleatoire(-1000,1000),force=10}
    else
      for i=1,#carte do
        if carte[i].nom=="bouffe" then
          return {move="pousse",x=carte[i].x,y=carte[i].y,force=200}
        end
      end
    end
    
    if love.math.random()<0.5 then
      return {move="pousse",x=aleatoire(-1000,1000),y=aleatoire(-1000,1000),force=100}
    else
      return {move="poison",x=x+aleatoire(-1000,1000),y=y+aleatoire(-1000,1000),force=10}
    end
    
  end
  
  
  
  
  function self.getXYT() return x,y,t end
  function self.getV() return vx,vy end
  function self.getAge() return age end

  return self
end



