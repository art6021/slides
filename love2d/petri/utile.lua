
-- nombre reel entre min et max
function aleatoire(min,max) return love.math.random()*(max-min)+min end


-- h : hue (0..1)
-- s : saturation (0..1) 0=pas de couleur, 1=max couleur
-- l : lightness (0..1) 0=noir, 0.5=max couleur 1=blanc
function couleurRandom() return HSL(love.math.random(),1,0.5) end
    
   
-- HSL vers RGB
function HSL(h, s, l)
  if s<=0 then return l,l,l end
	h=h*6
	local c = (1-math.abs(2*l-1))*s
	local x = (1-math.abs(h%2-1))*c
	local m,r,g,b = (l-.5*c), 0,0,0
	if h < 1     then r,g,b = c,x,0
	elseif h < 2 then r,g,b = x,c,0
	elseif h < 3 then r,g,b = 0,c,x
	elseif h < 4 then r,g,b = 0,x,c
	elseif h < 5 then r,g,b = x,0,c
	else              r,g,b = c,0,x
	end return (r+m),(g+m),(b+m)
end

-- cycle t entre min et max... le cycle se repete a tout les +1 de t
function cycle(t,min,max) return (math.sin(t*2*math.pi)+1)/2*(max-min)+min end


function norme(x,y) return math.sqrt(x*x+y*y) end
-- normalise pour une taille d (ou 1 par default)
function normalise(x,y,d)
  local n=norme(x,y)
  if n>0 then return x/n*(d or 1),y/n*(d or 1),n else return 0,0,n end
end

-- retourne l'angle du vecteur
function direction(vx,vy)
  return math.atan2(vy,vx)
end



function cycle(t,from,to) return (math.sin(t*2*math.pi)/2+0.5)*(to-from)+from end

--
-- pour le texte
--
function centreTexte(txt,font,x,y,scale)
  local w=font:getWidth(txt)
  love.graphics.setFont(font)
  love.graphics.print(txt,x,y,0,scale,scale,w/2,0)
end

function droiteTexte(txt,font,x,y,scale)
  local w=font:getWidth(txt)
  love.graphics.setFont(font)
  love.graphics.print(txt,x,y,0,scale,scale,w,0)
end



