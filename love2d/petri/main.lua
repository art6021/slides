
--
-- Petri
--
-- Des bactéries évoluent dans une boite de petri
--
-- La bactérie doit se nourrir des particules vertes, qui vont la faire grossir,
-- et doit éviter le poison, les particules rouges, qui vont a réduire.
--
-- Si une bactérie atteint sa taille la plus petite, elle peut être mangée par les bactéries
-- plus grosses, et disparaître... attention!
--
-- Une bactérie peut émettre du poison, histoire de rendre la vie des autres bactéries plus difficile...
-- (une bactérie est sensible à son propre poison... attention!)
--
-- La bactérie est un objet lua.
-- Elle a une position (x,y) et un rayon r, donc elle couvre environ la surface d'un cercle de rayon r.
-- Elle doit pouvoir se dessiner environ à cet endroit
-- Elle sera "mise a jour" avec un update()
-- Elle sera avertie d'avance des mouvements et évenements, de facon à pouvoir débuter une animation
-- De temps en temps, on va demander à la bactérie ce qu'elle veux faire dans la vie...
-- Les choix sont simples... se déplacer en direction d'un point (x,y) du Petri,
-- émettre un poison vers un point (x,y) du Petri, ou ne rien faire, évidemment.
--
--

io.stdout:setvbuf("no")



require("utile")
require("Camera")
require("Fond")
require("Bacterie")
require("Particules")
require("Bienvenue")
require("Message")

--require("Animatronic")

bacteries={
  require("BacterieYeux")
  ,require("BacterieActive")
  ,require("BacterieSimple")
}
--for i=2,5 do bacteries[i]=bacteries[1] end
  



function generateScoreBoard()
  local scores={}
  local txt1=""
  for i=1,#B do
    scores[i]={i=i,name=B[i].getName(),best=B[i].getAgeBest(),t=B[i].getT()}
  end
  table.sort(scores,function (a,b) return a.best>b.best end)
  return scores
end

--
-- la carte qu'on donne a la bacterie pour qu'elle decide son prochain move
-- C'est une liste des objets dans un rayon de 300 autour de la bacterie
-- Les objets possibles:
--   {nom="bacterie",x=x,y=y,t=t}  -- une bacterie de taille t a la position (x,y)
--   {nom="bouffe",x=x,y=y,t=t}  -- un delicieux repas de taille t
--   {nom="poison",x=x,y=y,t=t}  -- un poison a eviter, de taille t
function genereCarte(bact)
  local carte={}
  local x,y=bact.getXYT()
  P.vision(carte,x,y,300) -- les bouffes et les poisons
  for i=1,#B do
    if B[i]~=bact then
      local x,y,t=B[i].getXYT()
      carte[#carte+1]={nom="bacterie",x=x,y=y,t=t}
    end
  end
  --for i=1,#carte do print(i," : ",carte[i].nom,carte[i].x,carte[i].y,carte[i].t) end
  return carte
end

-- Ajoute un move dans la liste des moves a faire eventuellement.
function ajouteMove(bact,move)
  move.bact=bact
  move.duree=move.duree or 1 -- le move va s'executer dans 1 seconde
  M[#M+1]=move
  -- on averti la bacterie pour qu'elle puisse faire une animation "avant" le move
  if move.move=="deplace" then
    bact.prepareDeplace(move.duree,move.x,move.y)
  elseif move.move=="poison" then
    bact.preparePoison(move.duree,move.x,move.y,move.taille)
  elseif move.move=="mort" then
    bact.prepareMort(move.duree)
  end
end

 --
 -- generer un poison!
 --
 -- le poison est dans la direction x,y, alors faire attention!!
 --
function generePoison(bact,x,y,taille)
    local bx,by,bt=bact.getXYT()
    local bvx,bvy=bact.getV()
    bvx,bvy=bx-x,by-y
    taille=math.min(taille or aleatoire(5,10),bt-5)
    if taille<5 then return end -- pas assez fort!
    local L=norme(bvx,bvy)
    bt=bt+taille+5 -- ajoute un peu d'espace
    bvx,bvy=bvx/L,bvy/L -- vitesse qui s'eloigne de la bacterie
    local px,py = bx-bvx*bt,by-bvy*bt
    if norme(px,py)+taille<1000 then
      P.ajoutePoison(px,py,-bvx*10,-bvy*10,taille)
      bact.grossi(-taille)
    end
end


------------------------------------------------------------------------
-- INITIALISATION
------------------------------------------------------------------------

function love.load()
  love.window.setMode(1000,800)
  love.window.setPosition(900,100)

  C=Camera()
  F=Fond(800,800)
  B={} -- bacteries
  P=Particules()
  I=Bienvenue()
  Msg=nil -- pour les messages
    
  M={}  -- liste des moves a faire...
   
  track=nil -- si on veut suivre une bacterie, on met ici la bacterie
  displayScore=false -- clic gauche pour toggle, si on clic dans le vide
  
  --[[
  B[1]=Bacterie({x=0,y=0,t=50})
  B[2]=Bacterie({x=400,y=0,t=50})  
  B[3]=Bacterie({x=-400,y=0,t=50})  
  ]]
  
  fontInfo1=love.graphics.newFont("Ubuntu-BI.ttf",32)
  fontInfo2=love.graphics.newFont("Ubuntu-M.ttf",24)
  
  --camera=love.math.newTransform()
  
  bord=50 -- epaisseur du bord du petri

  -- vitesse generale
  -- si speed>=0, le facteur sur dt est  (speed+1)
  -- si speed<=0, le facteur sur dt est 1/(1-speed)
  speed=0
  
  -- -1000 .. 1000
  -- -1000 .. -400  ..  400 .. 1000
  --          0 ..     800
  C.reset(-1000-bord*2,1000+bord*2,-1000-bord*2,1000+bord*2)
  
  time=0 -- temps global
  
  --Z=Animatronic(0,10,10)
end



------------------------------------------------------------------------
-- DRAW
------------------------------------------------------------------------

function love.draw()
  C.apply()
  F.draw()
  P.draw()
  for i=1,#B do B[i].draw() end
  
  -- le petri..
  love.graphics.setColor(1,1,1,0.2)
  love.graphics.circle("fill",0,0,1000)
  love.graphics.setColor(1,1,1,1)
  love.graphics.setLineWidth(bord)
  love.graphics.circle("line",0,0,1000+bord/2)

  -- Bienvenue!
  if I then I.draw() end
  -- Message!
  if Msg then Msg.draw() end
  
  -- affiche l'info sur la bacterie courante
  love.graphics.push()
  love.graphics.origin()
  if track then
    local s={track.infoTexte()}
    love.graphics.setColor(1,0.4,0.4)
    love.graphics.setFont(fontInfo1)
    love.graphics.print((s and s[1]) or "pas de nom",20,20)
    love.graphics.setFont(fontInfo2)
    love.graphics.print((s and #s>1 and table.concat(s,"\n",2,#s)) or "???",20,60)
  end
  if displayScore then
    local scores=generateScoreBoard()
    local w,h=love.window.getMode()
    local th=fontInfo2:getHeight()
    love.graphics.setFont(fontInfo2)
    love.graphics.setColor(1,0.4,0.4)
    for i=1,#scores do
      love.graphics.print(scores[i].name,20,h-th*i)
      droiteTexte(string.format("%4ds",scores[i].best),fontInfo2,180,h-th*i,1)
    end
  end
  -- affiche la vitesse
  if speed~=0 then
    local w,h=love.window.getMode()
    love.graphics.setColor(1,0.8,0.4)
    local s
    if speed>0 then s="x "..(1+speed) else s="x 1/"..(1-speed) end
    droiteTexte(s,fontInfo1,w-20,10,1)
  end
  love.graphics.pop()
end


------------------------------------------------------------------------
-- UPDATE
------------------------------------------------------------------------

function love.update(dt)
  time=time+dt  -- le vrai temps, pas affecte par speed

  -- vitesse globale! de -5 a 5, 0=normal
  if speed>=0 then
    dt=dt*(1+speed)
  else
    dt=dt/(1-speed)
  end

  -- quelle est la plus grosse bacterie?
  local biggest=nil
  if #B>0 then
    biggest=1
    for i=2,#B do
        if B[i].getT() > B[biggest].getT() then biggest=i end
    end
  end

  -- update les bacteries
  for i=1,#B do B[i].update(dt,i==biggest) end
  
  -- ajuste pour les collisions entre bacteries
  for i=1,#B do
    for j=1,#B do
      if i~=j then B[i].obstacle(B[j].getXYT()) end
    end
  end
  
  -- deplace les particules (bouffe/poison)
  P.update(dt)
  -- elimine la bouffe vue par les bacteries
  for i=1,#B do
    local res=P.elimine(B[i].getXYT())
    for k,v in pairs(res) do
      if k=="bouffe" then B[i].grossi(v) end
      if k=="poison" then B[i].grossi(-3*v) end
    end
  end
  
  --
  -- demande un move a la bacterie
  --
  for i=1,#B do
    local age=B[i].getAge()
    if age%2 > (age+dt)%2 then
      local carte=genereCarte(B[i])
      local move=B[i].conscience(carte)
      if move then
        print("bacterie "..i.." move ",move.move)
        ajouteMove(B[i],move)
      end
    end
  end
  
  --
  -- execute les moves qui attendent
  -- preserve l'ordre FIFO! On ajoute a la fin.
  -- pour que #M fonctionne, on va mettre 0 dans un element traite
  --
  for i=1,#M do
    m=M[i]
    if m.bact.isDead() then
      M[i]=0
    elseif m.duree<=0 then
      if m.move=="deplace" then
        m.bact.deplace(m.x,m.y)
      elseif m.move=="poison" then
        generePoison(m.bact,m.x,m.y,m.taille)
        m.bact.poison(m.x,m.y,m.taille)
      elseif m.move=="mort" then
        m.bact.die()
        if not Msg then Msg=Message() end
        Msg.set(m.bact.getName(),"... est morte")
        C.softReset(-1000-bord*2,1000+bord*2,-1000-bord*2,1000+bord*2) -- ZOOM OUT!
      end
      M[i]=0 -- elimine le move
    else
      m.duree=m.duree-dt
    end
  end
  -- enleve les 0
  local j=1
  for i=1,#M do
    if type(M[i])=="table" then
      M[j]=M[i]
      j=j+1
    end
  end
  for i=j,#M do M[i]=nil end
  
  
  if track then C.track(track.getXYT()) end
  C.update(dt)
  
  if I then
    I.update(dt)
    if I.fini then
      I=nil
    end
  end
  
  if Msg then
    Msg.update(dt)
    if Msg.fini then Msg=nil end
  end
  
  -- elimine les bacteries mortes
  for i=#B,1,-1 do
    if B[i].isDead() then
      if track==B[i] then track=nil end -- fini le tracking
      B[i]=B[#B]
      B[#B]=nil
    end
  end
  
  
  -- nouvelle bacterie aux 2 secondes environ
  if not I and #B<#bacteries and love.math.random()<dt/2 then
    local L=math.sqrt(2)*(1000-20-50)
    local i=#B+1
    B[i]=Bacterie({x=aleatoire(-L,L),y=aleatoire(-L,L),t=50,bacterie=bacteries[i]})
  end
  
  -- detecte les bacteries qui doivent mourrir,  taille<10
  for i=1,#B do
    if B[i].getT()<10 and B[i].getAge()>10 then
      ajouteMove(B[i],{move="mort",duree=2})
    end
  end
  
  
end


------------------------------------------------------------------------
-- Souris & Clavier
------------------------------------------------------------------------


function love.mousepressed(x,y,b)
  local xx,yy = C.fenetre2monde(x,y)
  if b==1 then
    if I then I.done() return end
    if Msg then Msg.done() return end

    -- on clic sur une bacterie?    
    if track then track.select(false) end
    track=nil -- si on clic dans le vide, fin du tracking
    for i=1,#B do if B[i].contient(xx,yy) then track=B[i]  B[i].select(true) end end
  elseif b==2 then
    -- on a clic dans le vide, toggle displayscore
    displayScore=not displayScore
  elseif b==3 then
    --generePoison(B[1])
  end
end


function love.wheelmoved(x,y)
  local mx,my=love.mouse.getPosition()
  C.zoom(mx,my,y)   -- ajuste le zoom de la camera
end


function love.keyreleased(key)
  if key=="escape" then
    os.exit(0)
  end
  
  if I and key=="space" then I.done() end
  if Msg and key=="space" then Msg.done() end
  if key=="right" and speed<4 then speed=speed+1 end
  if key=="left" and speed>-4 then speed=speed-1 end
  
  -- move manual
  if track then
    -- on track cette bacterie... move manual!!
    if key=="up" then
      local xx,yy = C.fenetre2monde(love.mouse.getPosition())
       ajouteMove(track,{move="deplace",x=xx,y=yy})
    elseif key=="down" then
      local xx,yy = C.fenetre2monde(love.mouse.getPosition())
      ajouteMove(track,{move="poison",x=xx,y=yy,taille=5})
    end
  end
  
end
