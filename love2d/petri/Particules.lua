--
-- fond
--
require("utile")
require("Particule")


function Particules()
  local self={}
  local P={}
  for i=1,20 do
    P[i]=Particule()
  end
  
  function self.draw()
    for i=1,#P do
      P[i].draw()
    end
  end
  
  function self.update(dt)
    for i=1,#P do
      P[i].update(dt)
    end
    --
    -- nouvelle bouffe
    --
    if love.math.random()<dt and #P<500 then
      --print("New bouffe!!!")
      P[#P+1]=Particule({nom="bouffe",t=0,tmax=aleatoire(4,8)})
    end
  end
  
  -- enleve les particules qui sont dans une region de r autour de x,y
  -- retourne la taille totale des particules, pour chaque nom
  function self.elimine(x,y,r)
    local resultat={}
    for i=#P,1,-1 do
      if P[i].intersecte(x,y,r) then
        local q=P[i].getNom()
        resultat[q]=(resultat[q] or 0)+P[i].getTaille()
        table.remove(P,i)
      end
    end
    return resultat
  end
  
  
  function self.ajoutePoison(x,y,vx,vy,t)
    P[#P+1]=Particule({x=x,y=y,vx=vx,vy=vy,t=t,nom="poison"})
  end
  
  -- ajoute toutes les particules visibles dans la carte
  function self.vision(carte,x,y,rayon)
    for i=1,#P do
      if P[i].distance(x,y)<rayon then
        local xx,yy,tt=P[i].getXYT()
        carte[#carte+1]={nom=P[i].getNom(),x=xx,y=yy,t=tt}
      end      
    end
    
  end
  
  
  return self
end




