--
-- camera
-- la camera gere un carre dans le monde (xmin..xmax,ymin..ymax)
-- on peut deplacer le carre, scale, etc...
--
-- reset le terrain de jeux : self.reset(nxmin,nxmax,nymin,nymax)
-- applique la camera : self.apply()
-- tente de faire que la camera place x,y au centre  : self.track(x,y)
-- retourne la position dans le terrain de jeux d'une position dans la fenetre: self.fenetre2monde(x,y)
-- zoom autour du point (x,y) : self.zoom(x,y,dir)
-- update pour les animation:  self.update(dt)
-- 

function Camera()
  local self={}
  local xmin,xmax,ymin,ymax=0,100,0,100
  local target={duree=0} -- si on definit ca, c'est pour une animation
  local T=love.math.newTransform()
  local vxmin,vxmax,vymin,vymax=0,0,0,0 -- facteur 0.05

  local function computeT()
    local w,h = love.window.getMode()
    local q=math.min(w,h)
    T:reset()
    T:translate((w-q)/2,(h-q)/2) -- a droite, centre vertical
      -- 0..q, 0..q
     T:scale(q/w,q/h)
    -- 0 .. w    ,  0 .. h
    T:scale(w/(xmax-xmin),h/(ymax-ymin))
    -- 0 .. xmax-xmin    ,  0 .. ymax-ymin
    T:translate(-xmin,-ymin)
    -- xmin .. xmax    ,  ymin .. ymax
  end
  
  -- trim dans -1000..1000
  local function trim(min,max)
    if max-min>2200 then min,max=-1100,1100 end
    local d=math.min(0,min+1100)+math.max(0,max-1100)
    return min-d,max-d
  end
  
  -- defini le "terrain de jeux"
  function self.reset(nxmin,nxmax,nymin,nymax)
    xmin,xmax,ymin,ymax=nxmin,nxmax,nymin,nymax
    computeT()
  end
  function self.softReset(nxmin,nxmax,nymin,nymax)
    target.xmin=nxmin
    target.xmax=nxmax
    target.ymin=nymin
    target.ymax=nymax
    target.duree=5
  end
  
  function self.apply()
      -- 0..w , 0..h
      -- remap xmin..xmax -> 0 .. w,   et ymin..ymax -> 0 .. h
      love.graphics.applyTransform(T)
  end
  
  -- tente de faire que la camera place x,y au centre
  function self.track(x,y)
    local dx,dy,nxmin,nxmax,nymin,nymax
    if target.duree>0 then
      dx,dy=x-(target.xmax+target.xmin)/2,y-(target.ymax+target.ymin)/2
      nxmin,nxmax=trim(target.xmin+dx,target.xmax+dx)
      nymin,nymax=trim(target.ymin+dy,target.ymax+dy)
    else
      dx,dy=x-(xmax+xmin)/2,y-(ymax+ymin)/2
      nxmin,nxmax=trim(xmin+dx,xmax+dx)
      nymin,nymax=trim(ymin+dy,ymax+dy)
    end
    target.xmin=nxmin
    target.xmax=nxmax
    target.ymin=nymin
    target.ymax=nymax
    target.duree=0.5
  end

  -- retourne la position dans le terrain de jeux d'une position dans la fenetre
  function self.fenetre2monde(x,y)
    return T:inverseTransformPoint(x,y)
  end
  
  
  -- zoom la camera, 1 ou -1 pour la direction
  -- le zoom max est xmin=0 xmax=w ymin=0 ymax=h
  -- le x,y est la position du scale (mouse)
  function self.zoom(x,y,dir)
    x,y=T:inverseTransformPoint(x,y) -- point de reference pour le scale
    local f=1.1
    if dir>0 then f=1/f end
    local Z=love.math.newTransform()
    Z:translate(x,y)
    Z:scale(f)
    Z:translate(-x,-y)
    local nxmin,nxmax,nymin,nymax
    if target.duree>0 then
      -- animation en cours... on prend le point final, meme si on est pas encore la
      nxmin,nymin = Z:transformPoint(target.xmin,target.ymin)
      nxmax,nymax = Z:transformPoint(target.xmax,target.ymax)
    else
      nxmin,nymin = Z:transformPoint(xmin,ymin)
      nxmax,nymax = Z:transformPoint(xmax,ymax)
    end
    nxmin,nxmax=trim(nxmin,nxmax)
    nymin,nymax=trim(nymin,nymax)
    -- animation!
    target.xmin=nxmin
    target.xmax=nxmax
    target.ymin=nymin
    target.ymax=nymax
    target.duree=0.5
    --computeT()
  end
  
  function self.update(dt)
    if target.duree>0 then
      local f=math.min(dt/target.duree,1)
      xmin=(1-f)*xmin+(f)*target.xmin
      xmax=(1-f)*xmax+(f)*target.xmax
      ymin=(1-f)*ymin+(f)*target.ymin
      ymax=(1-f)*ymax+(f)*target.ymax
      target.duree=target.duree-dt
      computeT()
    end    
  end
  
  return self
end

