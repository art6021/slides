--
-- animation
--
-- anime une valeur de debut a fin pendant une certaine duree
-- si la duree est nil, on utilise le facteur, et pas de fin...
--

local A={}
setmetatable(A, {__mode = "v"})   -- les anims sont weak...

function updateAnimatronic(dt)
  print("** animatronic ",#A," animations **")
  for i=#A,1,-1 do
    local a=A[i]
    print(" update a is ",a.val)
    if a.duree then
      a.val=a.val+(a.fin-a.val)/a.duree*dt
      a.duree=a.duree-dt
      if a.duree<=0 then a.val=a.fin  a.fini=true  A[i]=A[#A] A[#A]=nil  end
    else
      a.val=a.facteur*a.val+(1-a.facteur)*a.fin
    end
  end
end


function Animatronic(debut,fin,duree,facteur)
  local self={val=debut,fin=fin,duree=duree,facteur=facteur}
  function self.fini() return self.duree and self.duree<=0 end
  A[#A+1]=self
  return self
end



--[[
a=Animation(100,200,10)
for i=1,20 do
  a.update(0.5)
  print("anim time=",i," val = ",a.val)
end

a=Animation(100,200,nil,0.95)
for i=1,30 do
  a.update(0.1 )
  print("anim time=",i," val = ",a.val)
end
]]

