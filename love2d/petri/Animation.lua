--
-- animation
--
-- anime une valeur de debut a fin pendant une certaine duree
-- si la duree est nil, on utilise le facteur, et pas de fin...
--

function Animation(debut,fin,duree,facteur)
  local self={}
  local debut=debut
  local fin=fin
  local duree=duree
  local facteur=facteur
  self.val=debut
  function self.fini() return duree and duree<=0 end
  function self.update(dt)
    if duree then
      self.val=self.val+(fin-self.val)/duree*dt
      duree=duree-dt
    else
      self.val=facteur*self.val+(1-facteur)*fin
    end
  end
  return self
end



--[[
a=Animation(100,200,10)
for i=1,20 do
  a.update(0.5)
  print("anim time=",i," val = ",a.val)
end

a=Animation(100,200,nil,0.95)
for i=1,30 do
  a.update(0.1 )
  print("anim time=",i," val = ",a.val)
end
]]

