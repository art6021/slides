--
-- message
--
-- fadein, clic ou delais pour finir, fadeout
--

function Message(fadein,delais,fadeout)
  local self={}
  local alpha=0
  local fadeinRef=fadein or 1
  local delaisRef=delais or 3
  local fadeoutRef=fadeout or 1
  local fadein=nil -- non nil: on est en fadein
  local delais=nil -- non nil: on est en delais
  local fadeout=nil -- non nil: on est en fadeout
  local font1=love.graphics.newFont("Ubuntu-B.ttf",128)
  local font2=love.graphics.newFont("Ubuntu-RI.ttf",96)
  local font3=love.graphics.newFont("Ubuntu-M.ttf",96)
  local info={} -- contient une liste de {titre=,msg=} a afficher un apres l'autre

  self.fini=false
  
  function self.set(titre,msg)
    info[#info+1]={titre=titre,msg=msg}
  end  
  
  function self.draw()
    if fadein or delais or fadeout then
      love.graphics.setColor(0.7,0.7,1,alpha)
      if info[1].titre then centreTexte(info[1].titre,font1,0,-300,2) end
      love.graphics.setColor(0.7,0.7 ,1,alpha)
      if info[1].msg then centreTexte(info[1].msg,font2,0,0,1) end
    end
  end
  
  function self.update(dt)
    if fadein then
      alpha=alpha+dt/fadein
      if alpha>=1 then alpha=1 fadein=nil delais=delaisRef end
    elseif delais then
      delais=delais-dt
      if delais<=0 then delais=nil fadeout=fadeoutRef end
    elseif fadeout then
      alpha=alpha-dt/fadeout
      if alpha<=0 then alpha=0 fadeout=nil table.remove(info,1) end
    else
      if #info>0 then
        fadein=fadeinRef
        self.fini=false
      else
        self.fini=true
      end
    end
  end
  
  -- termine et passe au prochain message
  function self.done()
    if #info>0 then
      fadein=nil
      delais=nil
      fadeout=fadeoutRef
    end
  end

  return self
end


