--
-- bienvenue
--
-- fadein de 3 secondes
-- clic pour finir
-- fade out de 3 secondes
--



function Bienvenue()
  local self={}
  local alpha=0
  local fadein=3
  local fadeout=3
  local font1=love.graphics.newFont("Ubuntu-B.ttf",128)
  local font2=love.graphics.newFont("Ubuntu-RI.ttf",96)
  local font3=love.graphics.newFont("Ubuntu-M.ttf",96)


  self.fini=false
  
  function self.draw()
    love.graphics.setColor(0.7,0.7,1,alpha)
    centreTexte("ART 6021",font1,0,-750,2)
    love.graphics.setColor(0.7,0.7 ,1,alpha)
    centreTexte("On est tous dans le pétri...",font2,0,-500,1)
    
    love.graphics.setColor(0.8,0.8,1,alpha)
    love.graphics.setFont(font3)
    love.graphics.print("Instructions\n - Roulette de la souris : Zoom\n - Clic gauche : suivre une bactérie\n\nContrôle manuel d'une bactérie \n - UP : avance vers la souris\n - Down : empoisonne",-750,-300,0,1,1)
  
    love.graphics.setColor(1,0.5,1,alpha*cycle(time/3,0.5,1))
    centreTexte("( ESPACE pour démarrer )",font3,0+cycle(time/5,-20,20),650+cycle(time/4,-20,20),1)
  end
  
  function self.update(dt)
    if fadein then
      alpha=alpha+dt/fadein
      if alpha>=1 then alpha=1 end -- on garde le fadeint actif a l'infini...
    elseif fadeout then
      alpha=alpha-dt/fadeout
      if alpha<=0 then alpha=0 fadeout=nil self.fini=true end
    end
  end
  
  -- enleve le message tout de suite
  function self.done()
    fadein=nil
  end
  
  
  return self
end


