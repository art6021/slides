 --
-- bacterie generique
-- gère tout les comportements de base...
-- utilise un ojbet MaBacterie() pour l'affichage de la bacterie, ses animations, et son comportement
--
require("utile")
--require("Animation")

--require("BacterieBlobule")


function Bacterie(info)
  local self={}
  -- les parametres de base, incontournables...
  local x=(info and info.x) or 0
  local y=(info and info.y) or 0
  local t=(info and info.t) or 50
  local vx,vy = 0,0
  local vt = 0
  local selected=false
  local bacterie=nil
  if info and info.bacterie then
    bacterie=info.bacterie(self)  -- une nouvelle instance de bacterie personalisee!
  end
  
  -- anime la taille...
  vt=-t*math.log(0.1)
  t=0
  
  local age=0
  local ageBest=0  -- temps qu'on a ete la plus grosse bacterie
  local dead=false
  
  function self.select(f) selected=f end
  function self.die() dead=true end
  function self.isDead() return dead end

  function self.draw()
    if dead then return end
    bacterie.draw()
  end
  
  -- donne un peu d'info sur la bacterie...
  function self.infoTexte()
    if dead then
      return bacterie.nom,"...est morte :-("
    end
    if bacterie.info then
      return bacterie.nom,bacterie.info()
    else
      return bacterie.nom,
      string.format("Position (%6.1f,%6.1f)",x,y),
      string.format("Taille %6.2f",t),
      string.format("Vitesse (%6.1f,%6.1f)",vx,vy)
    end
  end
  

  
  function self.prepareDeplace(delais,px,py)
    bacterie.prepareDeplace(delais,px,py)
  end
  function self.preparePoison(delais,px,py,taille)
    bacterie.preparePoison(delais,px,py,taille)
  end
  function self.prepareMort(delais)
    bacterie.prepareMort(delais)
  end
  
  
  -- demarre un deplacement vers px,py. On devrait arriver en 1s environ
  function self.deplace(px,py)
    if t<10 then return end -- trop petit...
    bacterie.deplace(px,py)
    local dx,dy=px-x,py-y
    vx=vx-dx*math.log(0.046)
    vy=vy-dy*math.log(0.046)
  end
  
  function self.poison(px,py,t)
    bacterie.poison(px,py,t)
  end
  
  
  -----------------------------
  
  function self.update(dt,best)
    if dead then return end
    age=age+dt
    if best then ageBest=ageBest+dt end
    x=x+vx*dt
    y=y+vy*dt
    -- on avance gratuitement...
    if norme(vx,vy)>10 then
      vx=vx*math.exp(math.log(0.046)*dt)   -- 0.046 en realite
      vy=vy*math.exp(math.log(0.046)*dt)
    end
    
    t=t+vt*dt
    vt=vt*math.exp(math.log(0.1)*dt)
    
    -- animation 
    --if ouvre then ouvre.update(dt) end
    --if ferme then ferme.update(dt) end
    
    -- bord du petri
    local L=norme(x,y)
    if L>1000-t then
      vx,vy=0,0
      x,y = x/L*(1000-t),y/L*(1000-t)
    end
    
    --[[if shake then
      shake=shake-dt
      if shake<0 then shake=nil end
    end
    ]]
    bacterie.update(dt)
  end
  
  -- verifie qu'on est pas en collision avec le cercle (centre xy, rayon t)
  -- ajuste seulement la vitesse.
  -- collision non elastique, avec leger rebond pour empecher la superposition
  function self.obstacle(ox,oy,ot)
    local dx,dy=ox-x,oy-y
    local d=norme(dx,dy)
    if d>t+ot then return end
    -- collision! projete vx,vy sur dy,dx pour la composante perpendiculaire
    dx,dy=dx/d,dy/d
    -- d contient le rebond... juste pour eliminer la superposition
    d=math.min(0,d-t-ot)
    local v=vx*dy-vy*dx
    vx,vy=dx*d+dy*v,dy*d-dx*v
  end
  
  
  function self.grossi(delta)
    -- si la constante de reduction est k, on va se deplacer de -vt /log[k]
    -- donc -vt/log[0.95] = delta -> vt = -delta*log[0.95]
    local target=math.max(5,t+delta)
    delta=target-t
    vt=vt-delta*math.log(0.1)    
    --t=math.max(5,t+delta)
    -- on devait mentionner le tout a la bacterie , permettre une animation de grossisement/reduction
    --print("grossi delta=",delta," vt=",vt)
    bacterie.grossi(delta)
  end
  
  --
  -- on nous donne un portrait du monde...
  -- on doit donne un "move"
  --
  function self.conscience(carte)
    if dead then return nil end
    local m=bacterie.conscience(carte)
    -- force un move a etre exactement 65 pixels
    if m then
      if m.move=="deplace" then
        if t<10 then return nil end -- on va mourrir :-(
        local dx,dy,n=normalise(m.x-x,m.y-y,65) -- force 65 pixels
        if n==0 then return nil end
        m.x=x+dx
        m.y=y+dy
      elseif m.move=="poison" then
        -- un poison doit etre <30... on peut demander super gros, on va obtenir le max permis
        m.taille=math.max(10,math.min(20,m.taille))
      end
    end
    return m
  end
  
  
  -- est-ce que le point px,py est dans notre bacterie?
  function self.contient(px,py)
    return norme(x-px,y-py)<t
  end

  
  
  function self.getXYT() return x,y,t end
  function self.getV() return vx,vy end
  function self.getAge() return age end
  function self.getName() return bacterie.nom end
  function self.getAgeBest() return ageBest end
  function self.getT() return t end

  return self
end



