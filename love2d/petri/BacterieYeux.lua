 --
-- bacterie simple qui fait l' affichage et le comportement
--
-- Attention... on utilise local ici pour ne pas avoir de probleme de nom...
-- et on retourne la fonction a la fin
--
-- la bacterie doit avoir ces attributs:
--
-- self.draw() -> dessine la bacterie
-- self.update(dt) -> mettre a jour la bacterie (et ses animations)

-- self.nom -> le nom de la bacterie
-- self.info()  -> retourne du texte informatif sur l abacterie  (optionel)

-- self.prepareDeplace(delais,px,py) -> prepare a un deplacement vers (px,py) dans "delais" secondes.
-- self.preparePoision(delais,px,py,t) -> prepare un poison de taille t en (px,py) dans delais secondes
-- self.prepareMort(delais) -> la particule sera morte (et eliminee) dans delais secondes.
-- 
-- self.deplace(px,py) -> demarre un deplacement vers (px,py)
-- self.grossi(delta) -> averti que la bacterie va changer de taille (on ajoute delta)
-- self.poison(px,py,t) -> on genere un poison en (px,py), taille t



local function MaBacterie(parent)
  local self={}
  local base=parent -- c'est notre base de bacterie, pour x,y,t entre autres...
  self.nom="B. Occulus"
  local c={1,1,0} -- couleur!
  local regard=0 -- direction du regard
  local vregard=0; -- vitesse

  function self.draw()
    local x,y,t = base.getXYT()
    local vx,vy = base.getV()
    love.graphics.push()    
    
    love.graphics.translate(x,y)    
    -- rond de base
    love.graphics.setColor(c[1],c[2],c[3],0.6)
    love.graphics.circle("fill",0,0,t)
      
      local angle=direction(vx,vy)
      love.graphics.rotate(regard)
      
      local f=(t/4)/(t-t/4)
      love.graphics.rotate(0)
      love.graphics.rotate(f)
      love.graphics.setColor(1,1,1,0.6)
      love.graphics.circle("fill",(t-t/4),0,t/4)
      love.graphics.setColor(0,0,0,0.6)
      love.graphics.circle("fill",(t-t/8),0,t/8)
      love.graphics.rotate(-2*f)
      love.graphics.setColor(1,1,1,0.6)
      love.graphics.circle("fill",(t-t/4),0,t/4)
      love.graphics.setColor(0,0,0,0.6)
      love.graphics.circle("fill",(t-t/8),0,t/8)
    
    love.graphics.pop()
  end
  
  -- information a afficher quand on suit notre bacterie.
  -- retourner une ou plusieurs string
  function self.info()
    return "He!","Bonjour!"
  end
  
      
  -- averti la bacterie qu'elle va se deplacer dans delais secondes, avec comme objectif px,py
  function self.prepareDeplace(delais,px,py)
    print(string.format("** %s DEPLACE dans %.1fs vers (%d,%d)",self.nom,delais,px,py))
    local x,y,t=base.getXYT()
    local delta=direction(px-x,py-y)-regard
    if delta<-math.pi then delta=delta+math.pi*2 end
    if delta>math.pi then delta=delta-math.pi*2 end
    vregard=vregard-delta*math.log(0.05)
  end
  
  -- averti la bacterie qu'elle va emettre un poison danns delais secondes, a la position (px,py) et taille t
  function self.preparePoison(delais,px,py,t)
    print(string.format("** %s POISON dans %.1fs position (%d,%d) taille=%.1f",self.nom,delais,px,py,t))
  end
  
  function self.prepareMort(delais)
    print(string.format("** %s MORT dans %.1fs",self.nom,delais))
  end
  
  
  
  -- annonce qu'on debut un mouvement vers px,py.
  -- la vitesse va nous indiquer si on arrive a destination
  function self.deplace(px,py)
    print(string.format("** %s deplace vers (%d,%d)!",self.nom,px,py))
  end
  
  function self.grossi(delta)
    print(string.format("** %s grossi delta=%.1f",self.nom,delta))
  end
  
  function self.poison(px,py,t)
    print(string.format("** %s POISON (%d,%d) taille=%.1f",self.nom,px,py,t))
  end
  

  
  -------------------
  
  function self.update(dt)
    regard=regard+vregard*dt
    vregard=vregard*math.exp(math.log(0.05)*dt)
  end
  
  
  --
  -- on nous donne un portrait du monde...
  --
  -- les move possibles:
  -- "deplace" vers (x,y)  -> (x,y) donne l'objectif, mais on se deplace toujours d'environ 65px
  -- "poison" vers (x,y) taille t -> (x,y) donne la direction, la taille t sera minimum 10, maximum 30
  --
  function self.conscience(carte)
    local x,y,t=base.getXYT()
    table.sort(carte,function (a,b) return norme(a.x-x,a.y-y)<norme(b.x-x,b.y-y) end)
    local x,y,t=base.getXYT() -- notre taille et position actuelle
    if t>100 then
      return {move="poison",x=x+aleatoire(-1000,1000),y=y+aleatoire(-1000,1000),taille=100}
    else
      for i=1,#carte do
        if carte[i].nom=="bouffe" then
          return {move="deplace",x=carte[i].x,y=carte[i].y}
        end
      end
    end
    
    if love.math.random()<0.5 then
      return {move="deplace",x=aleatoire(-1000,1000),y=aleatoire(-1000,1000)}
    else
      return {move="poison",x=x+aleatoire(-1000,1000),y=y+aleatoire(-1000,1000),taille=100}
    end
    return nil
  end
  
  return self
end

-- retourne notre fonction bacterie
return MaBacterie

