--
-- Logo LÖVE
--

love.window.setMode(400,400)
love.graphics.captureScreenshot("allo.png")

function love.draw()
  -- le fond
  love.graphics.setColor(0.9,0.25,0.5)
  love.graphics.circle("fill",200,200,150)  
  love.graphics.setColor(0.3,0.6,0.9)
  love.graphics.arc("fill",200,200,150,-math.pi/4,math.pi*3/4)
  
  -- le coeur, en deux moitiées
  love.graphics.setColor(1,1,1)
  --
  --love.graphics.setPointSize(8)
  --love.graphics.line(200,300,50,225,120,80,200,150)
  --[[
  curve1 = love.math.newBezierCurve(200,300,50,225,120,80,200,150)
  curve2 = love.math.newBezierCurve(400-200,300,400-50,225,400-120,80,400-200,150)
  love.graphics.polygon("fill",curve1:render())
  love.graphics.polygon("fill",curve2:render())  
    ]]

  -- le coeur, en plus simple
  --
  
  -- lobe gauche du coeur
  love.graphics.circle("fill",200-35,200,50)
  -- lobe droit
  love.graphics.circle("fill",200+35,200,50)
  -- pointe du coeur
  love.graphics.polygon("fill",200,300,200-80,225,200+80,225)
  
end

