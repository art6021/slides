--
-- eprouvette
--



local marge=5

-- nb = nb de boules max
-- clr : couleur des boules (0..#couleurs) 0=vide
function eprouvette(x,y,r,nb,clr)
  print("-- eprouvette --")
  local self={}
  local x=x or 200
  local y=y or 200
  local r=r or 10
  local nb=nb or 4
  local c={}
  for i=1,nb do c[i]=clr or 0 end
  function self.draw()
    --print("draw ",x,y,r,nb)
    -- les boules
    for i=1,nb do
      if c[i]>0 then 
        love.graphics.setColor(couleurs[c[i]])
        love.graphics.circle("fill",x,y-(i-1)*2*r,r)
      end
    end
    -- eprouvette
    local rr=r+marge
    love.graphics.setLineWidth(3)
    love.graphics.setColor(1,1,1)
    love.graphics.arc("line","open",x,y,rr,0,math.pi)
    -- bord gauche
    local x2=x-rr
    local y2=y-(nb-1)*2*r-r
    love.graphics.line(x2,y,x2,y2)
    love.graphics.arc("line","open",x2-r,y2,r,-math.pi/3,0)
    -- bord droit
    local x2=x+rr
    local y2=y-(nb-1)*2*r-r
    love.graphics.line(x2,y,x2,y2)
    love.graphics.arc("line","open",x2+r,y2,r,math.pi,math.pi+math.pi/3)

    -- reference
    --love.graphics.setColor(1,1,1)
    --love.graphics.setPointSize(3)
    --love.graphics.points(x,y)
  end
  
  -- retourne la boule du dessus, et met sa couleur a 0
  -- retourne x,y,c,  ou nil si rien a enlever
  -- (mx,my) : mouse pos. check si mx,my dans la boule
  function self.pop(mx,my)
    for i=nb,1,-1 do
      if c[i]>0 then
        local x1=x
        local y1=y-(i-1)*2*r
        -- mouse dans la boule?
        if (mx-x1)*(mx-x1)+(my-y1)*(my-y1) <= r*r then
          local cc=c[i]
          c[i]=0
          return {x=x1,y=y1,c=cc}
        else
          return nil
        end
      end
    end
    return nil
  end
  
  function self.drop(mx,my,clr)
    local rr=r+marge
    local y1=y-(nb-1)*2*r-r
    if mx<x-rr or mx>x+rr or my<y1 or my>y+rr then
      return false
    end
    for i=1,nb do
      if c[i]==0 then
        c[i]=clr
        return true
      end
    end
    return false
  end
  
  return self
end


