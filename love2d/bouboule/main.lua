-- https://love2d-community.github.io/love-api/
--
-- bouboule
--
--

-- pour permettre l'affichage immediate
io.stdout:setvbuf("no")


require("eprouvette")

t=0  -- time

-- les eprouvettes
ep={}

couleurs={
  {1,0,0},{0,1,0},{0,0,1},
  {1,1,0},{0,1,1},{1,0,1}
  }

-- boule en mouvement en ce moment {x,y,c}
moving=nil

-- nombre aleatoire differents a chaque execution
math.randomseed(os.time())

-- pour permettre l'affichage immediate
io.stdout:setvbuf("no")


function love.load()
  print("--- bouboule ---")
  love.window.setMode(640,480)
  --love.window.setFullscreen(true)
  
  for i=1,5 do
    ep[i]=eprouvette(100+70*(i-1),300,15,4,i>4 and 0 or i)
  end
end

function love.draw()
  for i=1,#ep do ep[i].draw() end
  
  if moving then
    love.graphics.setColor(couleurs[moving.c])
    love.graphics.circle("fill",moving.x+love.mouse.getX(),moving.y+love.mouse.getY(),10)
  end
  
  
end


function love.update(dt)
end

function love.mousepressed(x, y, button, istouch)
  if not moving then
    -- test chaque eprouvette pour prendre une boule
    for i=1,#ep do
      local b=ep[i].pop(x,y)
      if b then
        print("boule x",b.x,"y",b.y,"c",b.c)
        moving=b
        b.x=b.x-x
        b.y=b.y-y
        break
      end
    end
  else
    -- test chaque eprouvette pour dropper la boule
    for i=1,#ep do
      if ep[i].drop(x,y,moving.c) then
        moving=nil
        break
      end
    end
  end
end





