--
-- puzzle15
--

io.stdout:setvbuf("no")

function mmm(x, y, r, g, b, a)
  return 1-r,1-g,1-b,1
end

-- rescale an image, with aspect ratio
function rescale(img,sx,sy)
  local w,h=img:getDimensions()
  local nw,nh=math.floor(w*sx),math.floor(h*(sy or sx))
  print(w,h,nw,nh)
  c=love.graphics.newCanvas(nw,nh)
  love.graphics.setCanvas(c)
  love.graphics.draw(img,0,0,0,sx,sy or sx)
  love.graphics.setCanvas()
  return c:newImageData()
end

-- rescale an image, with aspect ratio
function rescaleToSize(img,nw,nh)
  local w,h=img:getDimensions()
  local sx,sy=nw/w,nh/h
  return rescale(img,sx,sy)
end

-- decoupe une imagedata en tuiles
function chop(imd,nx,ny)
  local nw,nh=math.floor(imd:getWidth()/nx),math.floor(imd:getHeight()/ny)
  print("chop",nw,nh)
  local tiles={}
  for j=0,ny-1 do
    for i=0,nx-1 do
      local td=love.image.newImageData(nw,nh)
      td:paste(imd,0,0,i*nw,j*nh,nw,nh)
      table.insert(tiles,{x=i,y=j,im=love.graphics.newImage(td)})
      print(i,j)
    end
  end
  return tiles
end

t=0
tiles={}
sx,sy=100,100
space=3
nbx,nby=6,4
cx,cy=0,0  -- current square

function love.load()
  img0=love.graphics.newImage("vang.jpg")
  --im1=rescale(im0,0.25,0.3)
  imd1=rescaleToSize(img0,sx*nbx,sy*nby)
  img1=love.graphics.newImage(imd1)
  print(img1:getDimensions())
  
  tiles=chop(imd1,nbx,nby)
  -- retire la tuile 0
  for i=1,#tiles do
    if tiles[i].x==0 and tiles[i].y==0 then table.remove(tiles,i) break end
  end
  
  -- positions initiales (px,py)
  for _,v in ipairs(tiles) do
    v.px,v.py=v.x,v.y
    v.ax,v.ay=v.px,v.py
  end  

  love.window.setMode(nbx*sx+(nbx-1)*space,nby*sy+(nby-1)*space)
end


function love.draw()
  --love.graphics.draw(img1)
  for _,v in ipairs(tiles) do
      --love.graphics.draw(v.im,v.px*(sx+space),v.py*(sy+space))
      love.graphics.draw(v.im,v.ax,v.ay)
      local x,y=v.px*(sx+space),v.py*(sy+space)
      v.ax=v.ax*0.9+x*0.1
      v.ay=v.ay*0.9+y*0.1
  end
end

function love.update(dt)
  t=t+dt
end

function deplace(k)
  local oldcx,oldcy=cx,cy
  if k=="up" then
    if cy>0 then cy=cy-1 end
  elseif k=="down" then
    if cy<nby-1 then cy=cy+1 end
  elseif k=="left" then
      if cx>0 then cx=cx-1 end
  elseif k=="right" then
      if cx<nbx-1 then cx=cx+1 end
  end
  -- deplace tuile en c vers oldc
  for _,v in ipairs(tiles) do
    if v.px==cx and v.py==cy then
      v.px=oldcx
      v.py=oldcy
    end
  end
end


function love.keyreleased(k)
  print(k)
  if k=="escape" then os.exit(0) end
  deplace(k)
  if k=="0" then
      -- reset all pos
      for _,v in ipairs(tiles) do v.px=v.x; v.py=v.y end
      cx,cy=0,0
  elseif k=="m" then
    local move={"up","down","left","right"}
    for i=1,100 do
      deplace(move[math.random(1,4)])
    end
  end    
end


