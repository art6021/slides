--
-- serveur / client
--

io.stdout:setvbuf("no")


local socket = require "socket"

function love.load()
  etat="demande"
end


function love.draw()
  if etat=="demande" then
    love.graphics.print("1 : serveur\n2 : client\n3 : quit",50,50)
  elseif etat=="serveur" then
    love.graphics.print("-- serveur --\n3 : quit",50,50)
    local info="--clients--\n"
    for uid,c in pairs(clients) do
      info=info..uid.." ip="..c.ip.." port="..c.port.." n="..c.n.."\n"
    end
    love.graphics.print(info,50,100)
    if cx then
      love.graphics.setColor(1,1,1)
      love.graphics.circle("fill",cx,cy,40)
    end

  elseif etat=="client" then
    love.graphics.print("-- client --\n3 : quit",50,50)
  else
  end
  
end

function love.update(dt)
  if etat=="serveur" then
    while true do
      -- receive
      local data, ip, port = udp:receivefrom()
      if data == nil then break end
      -- manage clients
      local uid = ip..":"..port
      if not clients[uid] then
        clients[uid] = { ip = ip, port = port, n=0 }
      end
      clients[uid].n=clients[uid].n+1
      -- re-broadcast
      for _, c in pairs(clients) do
        udp:sendto(c.n..":"..uid..":"..data, c.ip, 6790)
      end
      -- analyse le message
      print("received ",data)
      local t={}
      for s in data:gmatch("%w+") do table.insert(t,s) end
      cx=tonumber(t[1])
      cy=tonumber(t[2])
      print("cxy",cx,cy)
    end
  elseif etat=="client" then
    repeat
      local data = udp:receive()
      if data then
        print("received ",data)
      end
    until not data
  end
end

function demarreServeur()
  etat="serveur"
  udp=socket:udp()
  udp:settimeout(0) -- non blocking read
  udp:setsockname("*", 6789)
  clients={}
end

function demarreClient()
  etat="client"
  udp = socket.udp()
  udp:settimeout(0)
  udp:setsockname("*", 6790) -- recevoir du serveur
  udp:setpeername("127.0.0.1",6789) -- destination serveur
end


function love.keyreleased(k)
  if k=="escape" or k=="3" then os.exit(0) end
  if etat=="demande" then
    if k=="1" then demarreServeur()
    elseif k=="2" then demarreClient()
    end
  end
end

function love.mousepressed(x,y,b)
  print(x,y)
  if etat=="client" then
    local msg=tostring(x).." "..tostring(y).." "..tostring(b)
    --udp:sendto(msg,"127.0.0.1",6789)
    udp:send(msg)
    print("sending ",msg)
  end
end



-- send messages
--[[
local input = {}
function love.textinput(text)
  table.insert(input, text)
end

function love.keypressed(key)
  if key == "backspace" then
    table.remove(input)
  elseif key == "return" then
    udp:send(table.concat(input))
    input = {}
  end
end

-- receive messages
local history = {}
function love.update(dt)
  repeat
    local data = udp:receive()
    if data then
      table.insert(history, 1, data)
    end
  until data
end

function love.draw()
  love.graphics.print(table.concat(input), 0, 0)
  love.graphics.print(table.concat(history, "\n"), 0, 16)
end
]]--


