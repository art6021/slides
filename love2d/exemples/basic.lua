
-- ffmpeg -r 30 -i ~/.local/share/love/exemples/couleur%03d.png  couleurs.mp4
-- ffmpeg -i couleurs.mp4 couleurs.gif
-- (bug si on converti direct)


function love.load()
  love.window.setMode(400,300)
  n=0
  love.graphics.captureScreenshot("basic.png")
end


function love.draw()
  love.graphics.scale(2,2)
  
  love.graphics.setColor(1,0.4,0.4)
  love.graphics.setPointSize(5)
  love.graphics.points(30,30)
  
  love.graphics.setColor(0,1,0)
  love.graphics.setLineWidth(20)
  love.graphics.setLineJoin("miter")
  love.graphics.line(20,100,100,30,180,80)
  
  love.graphics.setColor(0,0,1)  
  love.graphics.circle("fill",83,100,42)
  
  love.graphics.setColor(1,1,1)
  love.graphics.setLineWidth(5)
  love.graphics.rectangle("line",121,20,62,30)

  
end

