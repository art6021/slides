--
-- test du screenshot animé
--
-- pour convertir l'animation en video:
-- ffmpeg -r 30 -i ~/.local/share/love/exemples/capture%04d.png  out.mp4
-- ou en gif anime:
-- ffmpeg -r 30 -i ~/.local/share/love/exemples/capture%04d.png  out.mp4



function love.load()
  love.window.setMode(500,250)
  n=0
  font = love.graphics.newFont( "Ubuntu-M.ttf", 64)
end


function love.draw()
  love.graphics.setColor(1,0,0)
  love.graphics.setLineWidth(20)
  love.graphics.setLineJoin("miter")
  love.graphics.line(20,20,100+n,100,180,30)
  
  love.graphics.setColor(0,1,0.4)
  local s=string.format("Bonjour %04d",n)
  love.graphics.print( s,font,200, 150,-n/100,1,1,200,30)
  
  local s=string.format("capture%04d.png",n)
  if n<120 then
    love.graphics.captureScreenshot(s)
  else
    os.exit(0)
  end
  
  n=n+1
end

