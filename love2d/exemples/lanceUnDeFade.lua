
-- ffmpeg -r 30 -i ~/.local/share/love/exemples/couleur%03d.png  couleurs.mp4
-- ffmpeg -i couleurs.mp4 couleurs.gif
-- (bug si on converti direct)


function love.load()
  love.window.setMode(400,400)
  love.window.setPosition(1200,20)
  love.window.setTitle("Lance un dé!")
  --love.graphics.captureScreenshot("lanceUnDe.png")
  canvas=love.graphics.newCanvas()
  love.graphics.setCanvas(canvas)
  love.graphics.clear(0,0,0,1) -- pas necessaire si c'est noir
  love.graphics.setCanvas()
  --love.graphics.setBackgroundColor(1,0,0)
  n=9999
end


function deVersion4(de,px,py,taille,fond,devant)
  love.graphics.origin()
  love.graphics.translate(px,py)  -- position du de
  love.graphics.scale(taille,taille)      -- taille du de

  love.graphics.setColor(fond)
  love.graphics.rectangle("fill",-100,-100,200,200,30,30)
  love.graphics.setColor(devant)
  love.graphics.setLineWidth(10)
  love.graphics.rectangle("line",-100,-100,200,200,30,30)
  
  local dx=40
  local dy=40
  if de==6 then dy=50 end -- espace extra
  
  if de==1 or de==3 or de==5 then
    love.graphics.circle("fill",0,0,20)
  end
  if de>1 then
    love.graphics.circle("fill",-dx,-dy,20)
    love.graphics.circle("fill",dx,dy,20)
  end
  if de>3 then
    love.graphics.circle("fill",-dx,dy,20)
    love.graphics.circle("fill",dx,-dy,20)
  end
  if de==6 then
    love.graphics.circle("fill",-dx,0,20)
    love.graphics.circle("fill",dx,0,20)
  end  
end


function love.draw()
  --deVersion1()
  --deVersion2()
  --deVersion3()
  
  --print(love.graphics.getBlendMode())
  
  -- fade --
  love.graphics.setCanvas(canvas)
  love.graphics.setColor(0,0,0,0.01)
  love.graphics.rectangle("fill",0,0,400,400)

  
  if time>0.1 then
    if n>999 then n=0 end
    time=0  
    love.graphics.setCanvas(canvas)    
    
    local px=math.random(0,400)
    local py=math.random(0,400)
    local taille=math.random()/2+0.4
    local fond={math.random(),math.random(),math.random()}
    local devant={1-fond[1],1-fond[2],1-fond[3]}
    deVersion4(math.random(1,6),px,py,taille,fond,devant)
  end
  
  love.graphics.setCanvas()
  love.graphics.origin()
  love.graphics.setColor(1,1,1,1)
  love.graphics.draw(canvas)
  
  --[[
  if n<300 then
   local s=string.format("lanceUnDeFade%03d.png",n)
   love.graphics.captureScreenshot(s)
  end
  ]]
  n=n+1
end

time=0
function love.update(dt)
  time=time+1/60  --dt
end

function love.keyreleased(key)
  print("key ",key)
  local b=tonumber(key) or 0
  if b>=1 and b<=6 then de=b end
end


