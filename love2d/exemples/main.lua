--
-- exemples
--
-- Utiliser "return" pour faire un screenshot manuel
-- sur linux, l'image sera ici: /home/roys/.local/share/love/exemples/snap.png
--

-- pour permettre l'affichage immediate
io.stdout:setvbuf("no")

-- live coding
require('mobdebug').start()

--require("exemple1")
--require("exemple2")
--require("couleurs")
--require("basic")
--require("rebond")
require("lanceUnDe")
--require("lanceUnDeFade")
--require("lanceUnDeDepart")

-- cas special... on veut proteger la fonction si elle existe

local kr=love.keyreleased

function love.keyreleased(key,scan)
  if kr then kr(key,scan) end  -- pour le module
  if key=="return" then
    print("screenshot!")
    -- l'image sera dans ~/.local/share/love/exemples/snap.png
    love.graphics.captureScreenshot("snap.png")
  elseif key=="escape" then
    os.exit(0)
  end
end



