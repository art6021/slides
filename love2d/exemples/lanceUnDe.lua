
-- ffmpeg -r 30 -i ~/.local/share/love/exemples/couleur%03d.png  couleurs.mp4
-- ffmpeg -i couleurs.mp4 couleurs.gif
-- (bug si on converti direct)


function love.load()
  love.window.setMode(400,400)
  love.window.setPosition(1200,20)
  love.window.setTitle("Lance un dé!")
  --love.graphics.captureScreenshot("lanceUnDe.png")
  de=1 -- choix du de
  n=0
  angleAnim=0
end

function deVersion1()
  love.graphics.setColor(0.3,0.3,1)
  love.graphics.rectangle("fill",100,100,200,200,30,30)

  love.graphics.setColor(1,1,0.3)
  if de==1 then
    love.graphics.circle("fill",200,200,20)
  elseif de==2 then
    love.graphics.circle("fill",200-40,200-40,20)
    love.graphics.circle("fill",200+40,200+40,20)
  elseif de==3 then
    love.graphics.circle("fill",200-40,200-40,20)
    love.graphics.circle("fill",200,200,20)
    love.graphics.circle("fill",200+40,200+40,20)
  elseif de==4 then
    love.graphics.circle("fill",200-40,200+40,20)
    love.graphics.circle("fill",200+40,200+40,20)
    love.graphics.circle("fill",200-40,200-40,20)
    love.graphics.circle("fill",200+40,200-40,20)
  elseif de==5 then
    love.graphics.circle("fill",200-40,200+40,20)
    love.graphics.circle("fill",200+40,200+40,20)
    love.graphics.circle("fill",200-40,200-40,20)
    love.graphics.circle("fill",200+40,200-40,20)
    love.graphics.circle("fill",200,200,20)
  elseif de==6 then
    love.graphics.circle("fill",200-40,200-50,20)
    love.graphics.circle("fill",200-40,200,20)
    love.graphics.circle("fill",200-40,200+50,20)
    love.graphics.circle("fill",200+40,200-50,20)
    love.graphics.circle("fill",200+40,200,20)
    love.graphics.circle("fill",200+40,200+50,20)
  end  
end

function deVersion2()
  love.graphics.setColor(0.3,0.3,1)
  love.graphics.rectangle("fill",100,100,200,200,30,30)
  love.graphics.setColor(1,0.9,0.3)
  
  local dx=40
  local dy=40
  if de==6 then dy=50 end
  
  if de==1 or de==3 or de==5 then
    love.graphics.circle("fill",200,200,20)
  end
  if de>1 then
    love.graphics.circle("fill",200-dx,200-dy,20)
    love.graphics.circle("fill",200+dx,200+dy,20)
  end
  if de>3 then
    love.graphics.circle("fill",200-dx,200+dy,20)
    love.graphics.circle("fill",200+dx,200-dy,20)
  end
  if de==6 then
    love.graphics.circle("fill",200-dx,200,20)
    love.graphics.circle("fill",200+dx,200,20)
  end  
end

function deVersion3()
  love.graphics.translate(240,200)  -- position du de
  love.graphics.scale(0.7,0.7)      -- taille du de

  love.graphics.setColor(0.3,0.3,1)
  love.graphics.rectangle("fill",-100,-100,200,200,30,30)
  love.graphics.setColor(1,0.7,0.3)
  
  local dx=40
  local dy=40
  if de==6 then dy=50 end -- espace extra
  
  if de==1 or de==3 or de==5 then
    love.graphics.circle("fill",0,0,20)
  end
  if de>1 then
    love.graphics.circle("fill",-dx,-dy,20)
    love.graphics.circle("fill",dx,dy,20)
  end
  if de>3 then
    love.graphics.circle("fill",-dx,dy,20)
    love.graphics.circle("fill",dx,-dy,20)
  end
  if de==6 then
    love.graphics.circle("fill",-dx,0,20)
    love.graphics.circle("fill",dx,0,20)
  end  
end

function deVersion4(de,px,py,taille,fond,devant,angle,bordure)
  love.graphics.translate(px,py)  -- position du de
  love.graphics.scale(taille,taille)      -- taille du de
  love.graphics.rotate(angle or 0)

  love.graphics.setColor(fond)
  love.graphics.rectangle("fill",-100,-100,200,200,30,30)
  love.graphics.setColor(devant)
  if bordure then
    love.graphics.setLineWidth(10)
    love.graphics.rectangle("line",-100,-100,200,200,30,30)
  end
  
  
  local dx=40
  local dy=40
  if de==6 then dy=50 end -- espace extra
  
  if de==1 or de==3 or de==5 then
    love.graphics.circle("fill",0,0,20)
  end
  if de>1 then
    love.graphics.circle("fill",-dx,-dy,20)
    love.graphics.circle("fill",dx,dy,20)
  end
  if de>3 then
    love.graphics.circle("fill",-dx,dy,20)
    love.graphics.circle("fill",dx,-dy,20)
  end
  if de==6 then
    love.graphics.circle("fill",-dx,0,20)
    love.graphics.circle("fill",dx,0,20)
  end  
end


function love.draw()
  --deVersion1()
  --deVersion2()
  --deVersion3()
  
  
  -- version 4 est speciale.... de au hasard....
  --math.randomseed(seed)
  
  -- version de base
  --deVersion4(de,px,py,1.0,{0.3,0.3,1},{1,1,0.3},false)
  
    -- version simple
  --deVersion4(de,px,py,taille,{0.3,0.3,1},{1,1,0.3})

  -- version avancee
  --deVersion4(de,px,py,taille,fond,devant,angle)
  
  -- version super-avancee
  deVersion4(de,px,py,taille,fond,devant,angleAnim,true)
  
end

  px=math.random(0,400)
  py=math.random(0,400)
  taille=math.random()+0.5
  fond={math.random(),math.random(),math.random()}
  devant={1-fond[1],1-fond[2],1-fond[3]}
  angle=math.random()*math.pi*2
  de=math.random(1,6)

time=0
function love.update(dt)
  time=time+dt
  angleAnim=angleAnim+dt
  if time>2000 then
      px=math.random(0,400)
      py=math.random(0,400)
      taille=math.random()+0.5
      fond={math.random(),math.random(),math.random()}
      devant={1-fond[1],1-fond[2],1-fond[3]}
      angle=math.random()*math.pi*2
      de=math.random(1,6)

    time=0
    --local s=string.format("lanceUnDeHasard%03d.png",n)
    --love.graphics.captureScreenshot(s)
    n=n+1
  end
end

function love.keyreleased(key)
  print("key ",key)
  local b=tonumber(key) or 0
  if b>=1 and b<=6 then
    de=b
    local s=string.format("lanceUnDe%03d.png",de-1)
    love.graphics.captureScreenshot(s)
  end
end


function love.mousepressed(x,y)
      px=x
      py=y
      taille=math.random()+0.5
      fond={math.random(),math.random(),math.random()}
      devant={1-fond[1],1-fond[2],1-fond[3]}
      angle=math.random()*math.pi*2
      de=math.random(1,6)
end




