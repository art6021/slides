
-- ffmpeg -r 30 -i ~/.local/share/love/exemples/couleur%03d.png  couleurs.mp4
-- ffmpeg -i couleurs.mp4 couleurs.gif
-- (bug si on converti direct)


function love.load()
  love.window.setMode(200,200)
  n=0
end


function love.draw()
  a=n*math.pi/180
  s=(math.sin(a)*math.sin(a))*20
  d=50-s    -- distance du centre
  r=44+s    -- rayon
  
  love.graphics.setColor(1,1,1)
  love.graphics.setColorMask(true,false,false,true)
  
  love.graphics.origin()
  love.graphics.translate(100,100)
  love.graphics.rotate(a)
  
  love.graphics.circle("fill",d,0,r)
  
  love.graphics.setColorMask(false,true,false,true)
  love.graphics.rotate(math.pi*2/3)
  love.graphics.circle("fill",d,0,r)
  
  love.graphics.setColorMask(false,false,true,true)
  love.graphics.rotate(math.pi*2/3)
  love.graphics.circle("fill",d,0,r)

  n=(n+1)%360
  love.graphics.setColorMask()
  
  local f=string.format("couleur%03d.png",n)
  love.graphics.captureScreenshot(f)
end
