--
-- damier 8x8
--

io.stdout:setvbuf("no")

function love.load()
  love.window.setMode(400,400)
  --love.graphics.captureScreenshot("spiro2.png")
  r,g,b = 0,0,0
  sec=0
end


function love.draw()
  
  --love.graphics.setBackgroundColor(r,g,b)
  
  love.graphics.setLineWidth(3)
  x,y = love.mouse.getPosition()
  
  --[[
  for i=0,400,20 do
    love.graphics.setColor(i/400,0.4,1)
    love.graphics.line(i,0,400,i)
  end
  ]]
  
  x1,y1=0,100
  x2,y2=400,0
  x3,y3=300,400
  
  --x2,y2=love.mouse.getPosition()
  
  for i=0,400,20 do
    local f=i/400
    love.graphics.setColor(f,0.4,1)
    love.graphics.line(
      (1-f)*x1+(f)*x2,(1-f)*y1+(f)*y2,
      (1-f)*x2+(f)*x3,(1-f)*y2+(f)*y3)
  end
end

function love.update(dt)
  sec=sec+dt
  r=math.sin(r+sec)^2
  g=math.sin(g+1.1*sec)^2
  b=math.sin(b+0.9*sec)^2
end



