
--
-- utilitaires
--

--
-- test si un point est dans un triangle
-- le triangle: { p1x p1y p2x p2y p3x p3y }
--

function interieurTriangle(t,p)
  --print("test interieur triangle")
  local ax,ay,bx,by,cx,cy = unpack(t)
  local px,py = unpack(p)
  --print(ax,ay,bx,by,cx,cy,":",px,py)
  local abx=bx-ax
  local aby=by-ay
  local bcx=cx-bx
  local bcy=cy-by
  local cax=ax-cx
  local cay=ay-cy
  local iab=-(px-ax)*aby+(py-ay)*abx
  local ibc=-(px-bx)*bcy+(py-by)*bcx
  local ica=-(px-cx)*cay+(py-cy)*cax
  --print("iab",iab,"ibc",ibc,"ica",ica)
  if iab>=0 and ibc>=0 and ica>=0 then
    return true
  else
    return false
  end
end

--
-- centre et rayon d'un triangle a partir de 3 points
--
function cercleParam(x1,y1,x2,y2,x3,y3)
  x2=x2-x1
  y2=y2-y1
  x3=x3-x1
  y3=y3-y1
  local denom=-2*x3*y2 + 2*x2*y3
  local cx= x2*x2*y3 + y3*y2*y2 - y2*(x3*x3 + y3*y3)
  local cy=-x2*x2*x3 - x3*y2*y2 + x2*(x3*x3 + y3*y3)
  cx=cx/denom
  cy=cy/denom
  local r=math.sqrt((x2-cx)*(x2-cx)+(y2-cy)*(y2-cy))
  return cx+x1,cy+y1,r
end


function test()
  local t={0, 0, 1, 1, 2, 3}
  local cx,cy,r = cercleParam(unpack(t))
  print(cx,cy,r)
end

