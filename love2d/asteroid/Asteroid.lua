
require("util")

Asteroid={}
Asteroid.__index=Asteroid

function Asteroid.new(taille)
		local self={}
		setmetatable(self,Asteroid)
		local w,h=love.graphics.getDimensions()

		self.taille=taille
		self.vang=math.random(0.1,50.0/taille)
    -- position
		self.cx=math.random(20+taille,w-taille)
		self.cy=math.random(20+taille,h-taille)
		self.color={math.random(0.5,1),math.random(0.5,1),math.random(0.5,1)}
		local d=2500.0/taille
		self.vx=math.random(d/2.0,d)*(math.random(0,1)*2-1)
		self.vy=math.random(d/2.0,d)*(math.random(0,1)*2-1)
		local d=math.pi/8.0
		local t1=math.random(-d,d)
		local t2=math.pi*2/3+math.random(-d,d)
		local t3=-math.pi*2/3+math.random(-d,d)
		local p={ math.cos(t1),math.sin(t1),
				  math.cos(t2),math.sin(t2),
				  math.cos(t3),math.sin(t3) } 
		for k,v in ipairs(p) do p[k]=v*taille end
		self.points=p
		return self
end

function Asteroid.newWithPoints(pts,taille)
		local self={}
    setmetatable(self,Asteroid)

    local x1,y1,x2,y2,x3,y3 = unpack(pts)
    self.cx,self.cy,self.taille=cercleParam(x1,y1,x2,y2,x3,y3)
		--self.taille=taille
    --self.cx=(x1+x2+x3)/3
    --self.cy=(y1+y2+y3)/3

		self.vang=math.random(0.1,50.0/taille)
    -- position
    x1=x1-self.cx
    x2=x2-self.cx
    x3=x3-self.cx
    y1=y1-self.cy
    y2=y2-self.cy
    y3=y3-self.cy
		self.color={math.random(0.5,1),math.random(0.5,1),math.random(0.5,1)}
		local d=2500.0/taille
		self.vx=math.random(d/2.0,d)*(math.random(0,1)*2-1)
		self.vy=math.random(d/2.0,d)*(math.random(0,1)*2-1)
		self.points={x1,y1,x2,y2,x3,y3}
		return self
end

function Asteroid:update(dt)
		-- check si il est sorti de la fenetre
		self.cx=self.cx+self.vx*dt
		self.cy=self.cy+self.vy*dt
		local m=self.taille
		local w,h=love.graphics.getDimensions()
    -- teleportation si completement en dehors de l'écran
		if self.cx<-m and self.vx<0 then self.cx=w+m end
		if self.cy<-m and self.vy<0 then self.cy=h+m end
		if self.cx>w+m and self.vx>0 then self.cx=-m end
		if self.cy>h+m and self.vy>0 then self.cy=-m end
end

function Asteroid:print()
		print("asteroid cx,cy=",self.cx,self.cy)
		for k,v in pairs(self.points) do print(k,v) end
end

function Asteroid:draw()
    local tr = love.math.newTransform(self.cx,self.cy,t*self.vang)
  
    love.graphics.push()
    love.graphics.applyTransform(tr)
  
		--love.graphics.push()
		--love.graphics.translate(self.cx,self.cy)
		--love.graphics.rotate(t*self.vang)
		love.graphics.setColor(unpack(self.color))
		love.graphics.polygon('fill', self.points)
		love.graphics.pop()
    
    love.graphics.setColor(0.5,0.5,0.5)
    love.graphics.circle("line",self.cx,self.cy,self.taille)
    
    love.graphics.setColor(1,0,0)
    love.graphics.setPointSize(5)
    love.graphics.points(unpack(self:getPoints()))
end

-- retourne les vrais points 2D actuels de l'asteroid
function Asteroid:getPoints()
    tr = love.math.newTransform(self.cx,self.cy,t*self.vang)  
    local x1,y1 = tr:transformPoint(self.points[1],self.points[2])
    local x2,y2 = tr:transformPoint(self.points[3],self.points[4])
    local x3,y3 = tr:transformPoint(self.points[5],self.points[6])
    return {x1,y1,x2,y2,x3,y3}
end


function Asteroid:touche(p)
    -- test le cercle en premier... tres rapide
    local dx,dy=p[1]-self.cx,p[2]-self.cy
    local d2=dx*dx+dy*dy
    if d2>self.taille*self.taille then return false end
    -- test si dans le triangle
    local pts=self:getPoints()
    local interieur=interieurTriangle(pts,p)
    return interieur
end

function Asteroid:explose()
    print("explose!!")
    -- explose un asteroid en trois triangles
    -- barycentre
    local x1,y1,x2,y2,x3,y3 = unpack(self:getPoints())
    local x12=(x1+x2)/2
    local y12=(y1+y2)/2
    local x23=(x2+x3)/2
    local y23=(y2+y3)/2
    local x31=(x3+x1)/2
    local y31=(y3+y1)/2

    local a1=Asteroid.newWithPoints({x1, y1, x12, y12, x31, y31},self.taille/2)
    local a2=Asteroid.newWithPoints({x2, y2, x23, y23, x12, y12},self.taille/2)
    local a3=Asteroid.newWithPoints({x3, y3, x31, y31, x23, y23},self.taille/2)
    return a1,a2,a3
end




