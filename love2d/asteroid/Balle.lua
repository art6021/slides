


Balle={}
Balle.__index=Balle


function Balle.new(x,y,ang,bx,by,speed,vie)
  local self={}
  setmetatable(self,Balle)
  self.x=x
  self.y=y
  local tr = love.math.newTransform(0,0,ang)  
  self.vx,self.vy = tr:transformPoint(speed,0)
  self.vx=self.vx+bx
  self.vy=self.vy+by
  self.vie=4
  return self
end

function Balle:draw()
  -- fade 0.5 secondes avant de mourrir
  local f=1.0
  if self.vie<0.5 then
    f=self.vie/0.5
    if f<0 then f=0 end
  end
  love.graphics.setColor(1*f,1*f,0)
  love.graphics.points({self.x,self.y})
end

function Balle:update(dt)
  self.x=self.x+self.vx*dt
  self.y=self.y+self.vy*dt
 
  -- <0 = mort!!
  self.vie=self.vie-dt
  
  local w,h=love.graphics.getDimensions()
  if self.x<0 then self.x=w end
  if self.x>w then self.x=0 end
  if self.y<0 then self.y=h end
  if self.y>h then self.y=0 end
end

function Balle:print()
  print("balle ("..self.x..","..self.y..") v=("..self.vx..","..self.vy..")")
end


