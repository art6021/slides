-- https://love2d-community.github.io/love-api/
--
-- asteroid
--
--
-- test polygon
--

t=0  -- time

math.randomseed(os.time())

require("util")
require("Asteroid")
require("Vaisseau")
require("Balle")

--test()
--os.exit()

-- pour permettre l'affichage immediate
io.stdout:setvbuf("no")

local joystick
local leftB,middleB,rightB=nil,nil,nil

function love.load()
  print("--- asteroid ---")
  --love.window.setMode(1920,1080)
  love.window.setFullscreen(true)
  astres={}
  for i=1,4 do
    astres[i]=Asteroid.new(100)
    astres[i]:print()
  end
  ship=Vaisseau.new(20)
  tirs={}
    
  -- son
  sfxTir = love.audio.newSource("tir.wav", "static")
  sfxThrust = love.audio.newSource("thrust.wav", "static")
  sfxThrust:setLooping(true)
  sfxThrust:setVolume(0)
  
  love.audio.play(sfxThrust)

	-- joystock for android
  joystick = love.joystick.getJoysticks()
  if #joystick>0 then joystick=joystick[1] else joystick=nil end
end

function love.draw()
	-- asteroid
	for _,a in ipairs(astres) do a:draw() end
	-- ship
  ship:draw()
	-- tirs
	for _,a in ipairs(tirs) do a:draw() end

	--local width, height = love.graphics.getDimensions()
	--love.graphics.print("screen is "..width.." x "..height,10,20)
	if joystick then
		local axis1, axis2 = joystick:getAxes()
		--love.graphics.print(string.format("X %12.6f Y %12.6f",axis1,axis2),10,20)
	end

	--[[
    local joysticks = love.joystick.getJoysticks()
	love.graphics.print("joysticks "..#joysticks, 10, 40)
    for i, joystick in ipairs(joysticks) do
    end
	--]]
	if msg1 then love.graphics.print(msg1, 10,40) end
	--if joystick then msg2=string.format("%8s %8s %8s",leftB,middleB,rightB) end
	if msg2 then love.graphics.print(msg2, 10,60) end

end


function love.update(dt)
  t=t+dt
  -- les asteroides
  for _,a in pairs(astres) do
    a:update(dt)
  end
  -- elimine les asteroid a enlever
  local i=1
  while i<=#astres do
    if astres[i].remove then
      astres[i]=astres[#astres]
      table.remove(astres)
    end
    i=i+1
  end
  -- mouvement clavier
  if love.keyboard.isDown("left") then ship:turn(-1) end
  if love.keyboard.isDown("right") then ship:turn(1) end
  if love.keyboard.isDown("up") then ship:thrust() end
	-- mouvement sur android
	if leftB and rightB then ship:thrust()
	elseif leftB then ship:turn(-1)
	elseif rightB then ship:turn(1)
	end

  ship:update(dt)
  
  -- deplace les tirs
  for _,a in ipairs(tirs) do
      a:update(dt)
  end
  
  -- verifie les collisions balle-asteroide
  for _,t in pairs(tirs) do
    if t.vie>0 then
      for _,a in pairs(astres) do
        if not a.vaExploser then
          if t.vie>0 and a:touche({t.x,t.y}) then
            a.vaExploser=true
            t.vie=-1.0
          end
        end
      end
    end
  end
  
  
    
  -- execute les explosions
  for _,a in pairs(astres) do
    if a.vaExploser then
      a.remove=true
      if a.taille>30 then
        local a1,a2,a3 = a:explose()
        table.insert(astres,a1)
        table.insert(astres,a2)
        table.insert(astres,a3)
      end
      break
    end
  end
  
  -- elimine les balles a enlever
  local i=1
  while i<=#tirs do
    if tirs[i].vie<=0 then
      tirs[i]=tirs[#tirs]
      table.remove(tirs)
    end
    i=i+1
  end
    
end



function love.touchpressed(id, x, y, dx, dy, pressure)
	--love.keypressed("space")
	--msg1="touch ".."("..x..","..y..")"
	local w,h=love.graphics.getDimensions()
	if x<w/4 and y<h/2 then leftB=id
	elseif x>w*3/4 and y<h/2 then rightB=id
	else
			if not middleB then
				love.keypressed("space")
			end
			middleB=id
	end
end



function love.touchreleased(id, x, y, dx, dy, pressure)
	--love.keypressed("space")
	if leftB==id then leftB=nil
	elseif middleB==id then middleB=nil
	elseif rightB==id then rightB=nil
	end
end

function love.keypressed(key,scan)
  print("key",key,scan)
  if key=="escape" then love.event.quit(0) end
  if key=="space" then
    print("tir!",#tirs)
    if #tirs<5 then
      -- ajoute a la fin du tableau
      table.insert(tirs,ship:debuteTir())
      love.audio.play(sfxTir)
    end
  end
end



