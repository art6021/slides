


Vaisseau={}
Vaisseau.__index=Vaisseau



--
-- vaisseau, style triangle, 4 points
--

function Vaisseau.new(taille)
  local self={}
  setmetatable(self,Vaisseau)
    
  local a=-140/180*math.pi
  local px=math.cos(a)*taille
  local py=math.sin(a)*taille
  self.points={taille,0,px,py,px,-py}
  self.cx=300
  self.cy=300
  self.vx=0
  self.vy=0
  self.ang=0
  self.vang=0 -- vitesse angulaire
  self.acc=0 -- acceleration par en avant (selon l'angle)
  return self
end


function Vaisseau:turn(dir)
  self.vang=dir*3.0
end

function Vaisseau:thrust()
  self.acc=300.0
end

-- retourne les vrais points 2D actuels de l'asteroid
local function tournePoints(cx,cy,ang,p)
  local tr = love.math.newTransform(cx,cy,ang)
  local x1,y1 = tr:transformPoint(p[1],p[2])
  local x2,y2 = tr:transformPoint(p[3],p[4])
  local x3,y3 = tr:transformPoint(p[5],p[6])
  return {x1,y1,x2,y2,x3,y3}
end

function Vaisseau:getPoints()
  return tournePoints(self.cx,self.cy,self.ang,self.points)
end

function Vaisseau:getFlamme()
  return tournePoints(self.cx,self.cy,self.ang,self.flamme)
end



function Vaisseau:debuteTir()
    local tr = love.math.newTransform(self.cx,self.cy,self.ang)  
    local x,y = tr:transformPoint(self.points[1],self.points[2])
    return Balle.new(x,y,self.ang,self.vx,self.vy,120,4)
end


function Vaisseau:draw()
  local tr = love.math.newTransform(self.cx,self.cy,self.ang)  
  love.graphics.push()
  love.graphics.applyTransform(tr)
  --- vaisseau
  love.graphics.setColor(1,1,1)
	love.graphics.polygon('fill', self.points)
  
  -- anime la flamme
  local flammeSize=self.acc/300.0
  local fp={}
  for i=1,#self.points do fp[i]=self.points[i] end
  fp[1]=flammeSize*(-2*fp[1])+(1-flammeSize)*fp[3]
  fp[2]=math.random(-5,5)
  fp[4]=fp[4]*0.6
  fp[6]=fp[6]*0.6

  --fp[1]=(1-flammeSize)*(fp[1]+math.random(-5,5)) + flammeSize*fp[3]
  --fp[2]=math.random(-5,5)
  love.graphics.setColor(1,math.random(0,0.4),0)
	love.graphics.polygon('fill', fp)
	love.graphics.pop()
  
  --[[
  local p=self:getPoints()
  love.graphics.setColor(1,0,0)
  love.graphics.points(p)
  -- flamme
  local p=self:getFlamme()
  love.graphics.points(p)
  --]]
  
  sfxThrust:setVolume(self.acc/300.0)
  
end

function Vaisseau:update(dt)
  self.ang=self.ang+self.vang*dt
  if self.ang<-math.pi then self.ang=self.ang+2*math.pi end
  if self.ang>math.pi  then self.ang=self.ang-2*math.pi end
  
  local tr = love.math.newTransform(0,0,self.ang)
  local dx,dy = tr:transformPoint(self.acc*dt,0)
  self.vx=self.vx+dx
  self.vy=self.vy+dy
  self.cx=self.cx+self.vx*dt
  self.cy=self.cy+self.vy*dt
  
  local w,h=love.graphics.getDimensions()
  if self.cx<0 then self.cx=w end
  if self.cx>w then self.cx=0 end
  if self.cy<0 then self.cy=h end
  if self.cy>h then self.cy=0 end
    
  -- reduit la vitesse angulaire
  self.vang=self.vang*0.8
  self.acc=self.acc*0.8
  self.vx=self.vx*0.99
  self.vy=self.vy*0.99
end



