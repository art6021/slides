--
-- particule
-- utilise w,h, intervalle
-- peut servir a plusieurs choses (lancement, explosion, ...)
--

function Particule(init)
  local self={}
  local x=(init and init.x) or love.math.random(1,w)
  local y=(init and init.y) or love.math.random(1,h)
  local vx=(init and init.vx) or love.math.random()*10
  local vy=(init and init.vy) or love.math.random()*10
  local ax=(init and init.ax) or 0
  local ay=(init and init.ay) or 9.8
  local couleur=(init and init.couleur) or {1,1,1,1}
  local vie=(init and init.vie) or intervalle(1,3)
  local cintille=(init and init.cintille) or nil
  local flash=(init and init.flash) or nil
  local age=0
  local flashing=intervalle(20,30)
  
  self.draw = function ()
    love.graphics.setPointSize(5)
    local c={}  for i=1,#couleur do c[i]=couleur[i] end  -- copie de la couleur
    if cintille then
      c[4]=math.sin(age*2*math.pi*5)*0.2+0.9
    elseif vie>0 then
      c[4]=1-math.max(math.min(age/vie,1),0)
    end
    if flash then
      if age>flash and math.sin((age-flash)*flashing)>0 then
        c[1]=1  c[2]=1  c[3]=1
        c[4]=math.max(math.min(vie-age,1),0)
      end
    end
    
    love.graphics.setColor(c)
    love.graphics.points(x,y)
  end
  
  self.update = function (dt)
    x=x+vx*dt
    y=y+vy*dt
    vx=vx+ax*dt
    vy=vy+ay*dt
    age=age+dt
  end
  
  self.vivant = function () return age<=vie end
  self.position = function () return x,y end

  return self
end
