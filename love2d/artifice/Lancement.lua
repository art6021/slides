--
-- lancement
-- part en x1,y1 et va vers x2,y2 pour exploser apres t secondes
--

require("Particule")
require("Explosion")
require("ExplosionFlash")

function Lancement(init)
  self={}
  local P=nil -- lancement, une seule particule
  local E=nil -- explosion, une seule explosion
  local x1=(init and init.x1) or intervalle(0,w)
  local y1=(init and init.y1) or h
  local x2=(init and init.x2) or intervalle(0,w)
  local y2=(init and init.y2) or intervalle(0,h/2)
  local hue=(init and init.hue) or intervalle(0,1)
  local r,g,b=HSL(hue,1,0.5)
  
  local P=Particule({x=x1,y=y1,vx=(x2-x1)/4,vy=(y2-y1)/3,ax=0,ay=40,couleur={r,g,b,1},cintille=true,vie=4})
    
  function self.draw()
    if P then P.draw() end
    if E then E.draw() end
  end
  
  function self.update(dt)
    if P then P.update(dt) end
    if E then E.update(dt) end
    if P and not P.vivant() then
      local x,y=P.position()
      if love.math.random()<0.2 then
        E=ExplosionFlash({x=x,y=y,hue=hue})
      else
        E=Explosion({x=x,y=y,hue=hue})
      end
      P=nil
    end
  end
  
  function self.vivant() return (P and P.vivant()) or (E and E.vivant()) end  
  
  return self
end

