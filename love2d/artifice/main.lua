--
-- feux d' artifice
--
io.stdout:setvbuf("no")

require("utile")
require("Lancement")


function intervalle(a,b) return love.math.random()*(b-a)+a end

L={}

function love.load()
  love.window.setMode(800,800)
  love.window.setPosition(1000,50)
  --love.window.setFullscreen(true)
  w,h=love.window.getMode()
end


function love.draw()
  for i=1,#L do L[i].draw() end
end

function love.update(dt)
  for i=#L,1,-1 do
    L[i].update(dt)
    if not L[i].vivant() then L[i]=L[#L] L[#L]=nil end
  end

  if #L<20 and love.math.random()<dt then
    L[#L+1]=Lancement()
  end
end

function love.keypressed(key)
  if key=="escape" then
    os.exit()
  elseif key=="space" then
    L={}
  end
  
end

