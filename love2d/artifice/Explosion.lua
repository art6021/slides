--
-- explosion
--

require("Particule")

function Explosion(init)
  self={}
  local P={} -- particules
  local x=(init and init.x) or intervalle(0,w)
  local y=(init and init.y) or intervalle(0,h)
  local hue=(init and init.hue) or intervalle(0,1)
  local vmin=(init and init.vmin) or 20
  local vmax=(init and init.vmax) or 150
  local r,g,b,a=HSL(hue,1,0.5)
  
  
  for i=1,100 do
    local t=love.math.newTransform(0,0,love.math.random()*2*math.pi,2,2)
    local vx,vy = t:transformPoint(intervalle(vmin,vmax),0)
    P[i]=Particule({x=x,y=y,vx=vx,vy=vy,ax=0,ay=400,couleur={r,g,b}})
  end
  
  function self.draw()
      for i=1,#P do P[i].draw() end
  end
  
  function self.update(dt)
    -- a l'envers pour pouvoir elminer des points en meme temps
    for i=#P,1,-1 do
      P[i].update(dt)
      if not P[i].vivant() then P[i]=P[#P] P[#P]=nil end
    end
  end
  
  function self.vivant() return #P>0  end  
  
  return self
end

