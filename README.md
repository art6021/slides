## quelques references

[www.love2d.org](https://loveref.github.io/)

-- CS50 game dev
https://youtu.be/GfwpRU0cT10

-- udemy - love2d
https://www.udemy.com/course/lua-love/

-- learn lua in 15min
http://tylerneylon.com/a/learn-lua/

-- pour le cell
https://love2d.org/wiki/Game_Distribution/APKTool

-- tutorial love2d
https://love2d.org/forums/viewtopic.php?f=3&t=84886

https://sheepolution.com/learn/book/contents

-- creer un .love
dans le repertoire SuperGame
zip -9 -r SuperGame.love .

-- jeux et exemples
https://itch.io/games/platform-android/tag-love2d

--- pour creer android version ---
cd asteroid
zip -9 -r ../asteroid.love .
cd ..

mv asteroid.love android/love_decoded/assets/game.love

cd android
apktool b -o asteroid.apk love_decoded
java -jar uber-apk-signer-1.1.0.jar -a asteroid.apk
adb install asteroid-aligned-debugSigned.apk

:



